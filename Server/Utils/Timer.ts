///<reference path='..\PlanetWarsServer.ts' />

module PlanetWars.Utils {
    export class Timer {

        static getTime(): number {
            return new Date().getTime();
        }

        static getElapsedTimeInSec(time: number, previousTime: number): number {
            return (time - previousTime) / 1000;
        }
    }
}
