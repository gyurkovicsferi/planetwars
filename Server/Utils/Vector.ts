///<reference path='..\PlanetWarsServer.ts' />

module PlanetWars.Utils {
    export class Vector {
        x: number;
        y: number;

        constructor (x: number = 0, y: number = 0) {
            this.x = x;
            this.y = y;
        }

        static vectorBetween(fromVector: Vector, toVector: Vector): Vector {
            return new Vector(toVector.x - fromVector.x, toVector.y - fromVector.y);
        }

        getDistanceFrom(otherVector: Vector): number {
            var distanceVector = Vector.vectorBetween(this, otherVector);
            return distanceVector.length();
        }

        length() {
            return Math.sqrt(this.x * this.x + this.y * this.y);
        }

        add(vector: Vector) {
            return new Vector(this.x + vector.x, this.y + vector.y);
        }

        mul(num: number) {
            return new Vector(this.x * num, this.y * num);
        }

        normalize() {
            var length = this.length();
            return new Vector(this.x / length, this.y / length);
        }

        getAngleFromVerticalUp() {
            var factor;
            if (this.x < 0) {
                factor = -1;
            } else {
                factor = 1;
            }
            return factor * Math.acos(-this.y / this.length());
        }

        static getAngleBetween(a: Vector, b: Vector): number {
            return Math.acos(this.dotProduct(a, b) / (a.length() * b.length()));
        }

        static dotProduct(a: Vector, b: Vector): number {
            return a.x * b.x + a.y * b.y;
        }

        rotate(angle: number) {
            var angleRad = Utils.DegRadConverter.ConvertToRad(angle),
            sinA = Math.sin(angleRad),
            cosA = Math.cos(angleRad);

            return new Vector(this.x * cosA - this.y * sinA, this.x * sinA + this.y * cosA);
        }

        rotateLeft() {
            return new Vector(this.y, -this.x)
        }

        rotateRight() {
            return new Vector(-this.y, this.x)
        }
    }
}