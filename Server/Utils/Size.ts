///<reference path='..\PlanetWarsServer.ts' />

module PlanetWars.Utils {
    export class Size {
        constructor (public width: number, public height: number) { }
    }
}