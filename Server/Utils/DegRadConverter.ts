///<reference path='..\PlanetWarsServer.ts' />

module PlanetWars.Utils {
    export class DegRadConverter {
        static ConvertToRad(degree: number): number {
            return degree * Math.PI / 180;
        }

        static ConvertToDeg(radian: number): number {
            return radian / Math.PI * 180;
        }
    }
}