module PlanetWars.Server {
    
    interface WebServerConfiguration {
        port: number;
        defaultFileName: string;
    }

    export class WebServer {
        private app;//: ExpressApplication;
        private server;
        private io;
        private express;

        constructor () {
            this.express = require('express');
            this.app = this.express();
            this.server = require('http').createServer(this.app);
            this.io = require('socket.io').listen(this.server);  
        }

        init(configuration) {
            var port = process.env.PORT || 8090,
                fileName = configuration.defaultFileName || '/index.html';

            this.initialiseHttpServer(port);
            this.configureHttpServer(fileName);

            //this.io.enable('browser client gzip');

            //this.io.set('transports', [
            //            'websocket',
            //            'flashsocket'
            //]);

            //this.io.enable('browser client minification');  // send minified client
            //this.io.set('log level', 1);                    // reduce logging
        }

        getSocketIO() {
            return this.io;
        }

        private initialiseHttpServer(port) {
            this.server.listen(port);
            console.log("Server is listening in " + port);
        }

        private handleGetCompleted(error) {
            if (error) {
                this.handleGetError(error);
            } else {
                this.handleGetSuccess();
            }
        }

        private handleGetError(error) {
            console.error("Routing error: " + error);
        }

        private handleGetSuccess() {
            console.log("Get was successful");
        }

        private configureHttpServer(fileName: string) {
            this.app.use(this.express.static(__dirname + '/public'));
        }
    }
}