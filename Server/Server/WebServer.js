///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    (function (Server) {
        var WebServer = (function () {
            function WebServer() {
                this.express = require('express');
                this.app = this.express();
                this.server = require('http').createServer(this.app);
                this.io = require('socket.io').listen(this.server);
            }
            WebServer.prototype.init = function (configuration) {
                var port = configuration.port || 8080, fileName = configuration.defaultFileName || '/index.html';

                this.initialiseHttpServer(port);
                this.configureHttpServer(fileName);

                this.io.enable('browser client gzip');
                //this.io.set('transports', [
                //            'websocket',
                //            'flashsocket'
                //]);
                //this.io.enable('browser client minification');  // send minified client
                //this.io.set('log level', 1);                    // reduce logging
            };

            WebServer.prototype.getSocketIO = function () {
                return this.io;
            };

            WebServer.prototype.initialiseHttpServer = function (port) {
                this.server.listen(port);
                console.log("Server is listening in " + port);
            };

            WebServer.prototype.handleGetCompleted = function (error) {
                if (error) {
                    this.handleGetError(error);
                } else {
                    this.handleGetSuccess();
                }
            };

            WebServer.prototype.handleGetError = function (error) {
                console.error("Routing error: " + error);
            };

            WebServer.prototype.handleGetSuccess = function () {
                console.log("Get was successful");
            };

            WebServer.prototype.configureHttpServer = function (fileName) {
                this.app.use(this.express.static(__dirname + '/public'));
            };
            return WebServer;
        })();
        Server.WebServer = WebServer;
    })(PlanetWars.Server || (PlanetWars.Server = {}));
    var Server = PlanetWars.Server;
})(PlanetWars || (PlanetWars = {}));
