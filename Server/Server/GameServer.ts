///<reference path='..\PlanetWarsServer.ts' />

module PlanetWars.Server {
    export class GameServer {
        
        private io;
        private webServer: WebServer;
        private static current: GameServer;
        private modelController: Model.ModelController;
        private players;

        static Start() {
            new GameServer();
        }

        constructor () {
            GameServer.current = this;
            
            this.initGame();
            this.initWebServer();
            this.initCommunication();
        }

        private initGame() {
            this.modelController = Model.GameFactory.newGame();
        }

        private initWebServer() {
            this.webServer = new WebServer();
            this.webServer.init({
                port: Settings.WebServer.port,
                defaultFileName: Settings.WebServer.startPage
            });
        }

        private initCommunication() {
            this.players = {};
            var that = this;

            this.io = this.webServer.getSocketIO();
            this.io.use(function(socket, next){            
                var player = that.players[socket.id];
                if (typeof player !== "undefined") {
                    socket.player = this.players[socket.id];
                }
                next();
            });

            this.io.sockets.on(Communication.messages.connection, (socket) => {
                var msg = Communication.messages;
                var dir = Communication.directions;

                this.initCreateNewPlayer(socket);
                this.initRefreshRequest(socket);

                this.initMessage(socket, msg.startMove, this.onStartMove);
                this.initMessage(socket, msg.endMove, this.onEndMove);
                this.initMessage(socket, msg.disconnect, this.onDisconnect);
                this.initMessage(socket, msg.startFire, this.onStartFire);
                this.initMessage(socket, msg.endFire, this.onEndFire);
            });
        }

        private initCreateNewPlayer(socket) {
            var msg = Communication.messages,
                that = this;

            socket.on(msg.createNewPlayer, (args) => {
                var player = that.modelController.gameController.createNewPlayer(args.name, args.group);
                var planetDtos = that.modelController.modelQueries.getPlanetsDto();
                

                console.log(args.name + " connected!");

                //socket.set(Settings.Communication.playerStorageName, player);
                this.players[socket.id] = player;
                socket.player = player;

                socket.emit(Communication.messages.newPlayerCreated, {
                    id: player.id,
                    p: planetDtos
                })
            });
        }

        private initRefreshRequest(socket) {
            var msg = Communication.messages,
                that = this;

            socket.on(msg.refreshRequest, (data) => {
                var player: Model.Player = socket.player;
                if (player !== null) {
                    player.fireAngle = data.angle;
                    var redscore = that.modelController.modelQueries.getScoreForGroup(0);
                    var bluescore = that.modelController.modelQueries.getScoreForGroup(1);

                    var items = that.modelController.modelQueries.getDtosForPlayer(player);
                    socket.emit(msg.refresh,
                        {
                            s: data.sendTime,
                            x: Math.round(player.position.x),
                            y: Math.round(player.position.y),
                            r: redscore,
                            b: bluescore,
                            i: items
                        });
                }
            });
        }

        private initMessage(socket, message: string, callback) {
            var that = this;
            socket.on(message, function (data) {
                that.loadPlayer(socket, function (err, player) {
                    if (player !== null) {
                        callback.call(that, err, player, data);
                    }
                });
            });
        }

        private onStartMove(err: string, player: Model.Player, data: any) {
            var dir = Communication.directions;

            if (data.d === dir.up) this.modelController.playerController.startMoveUp(player);
            if (data.d === dir.left) this.modelController.playerController.startMoveLeft(player);
            if (data.d === dir.right) this.modelController.playerController.startMoveRight(player);
        }

        private onEndMove(err: string, player: Model.Player, data: any) {
            var dir = Communication.directions;

            if (data.d === dir.up) this.modelController.playerController.endMoveUp(player);
            if (data.d === dir.left) this.modelController.playerController.endMoveLeft(player);
            if (data.d === dir.right)this.modelController.playerController.endMoveRight(player);
        }

        private onDisconnect(err: string, player: Model.Player, data: any) {
            console.log(player.name + " disconnected!");

            this.modelController.gameController.removePlayer(player);
        }

        private onStartFire(err: string, player: Model.Player, data: any) {
            this.modelController.playerController.startFire(player);
        }

        private onEndFire(err: string, player: Model.Player, data: any) {
            this.modelController.playerController.endFire(player);
        }

        private savePlayer(socket, player: Model.Player) {
            //socket.player = player;
        }

        private loadPlayer(socket, callback: (err: string, player: Model.Player) => void ) {
            callback(null, socket.player);
        }

        static publishPlanetAdded(planet: Model.Planet) {
            if (typeof this.current.io !== "undefined") {
                this.current.io.sockets.emit(Communication.messages.planetAddedName, planet.toDto());
            }
        }

        static publishPlanetRemoved(planet: Model.Planet) {
            if (typeof this.current.io !== "undefined") {
                if (typeof planet !== "undefined")
                    this.current.io.sockets.emit(Communication.messages.planetRemovedName, planet.toDto());
            }
        }
    }
}