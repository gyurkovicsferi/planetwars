///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    (function (Server) {
        (function (Communication) {
            Communication.messages = {
                connection: 'connection',
                disconnect: 'disconnect',
                createNewPlayer: 'p',
                newPlayerCreated: 'P',
                startMove: 'm',
                endMove: 'M',
                startFire: 'f',
                endFire: 'F',
                refreshRequest: 'r',
                refresh: 'r',
                planetAddedName: 'a',
                planetRemovedName: 'A'
            };

            Communication.directions = {
                up: 'u',
                down: 'd',
                left: 'l',
                right: 'r'
            };
        })(Server.Communication || (Server.Communication = {}));
        var Communication = Server.Communication;
    })(PlanetWars.Server || (PlanetWars.Server = {}));
    var Server = PlanetWars.Server;
})(PlanetWars || (PlanetWars = {}));
