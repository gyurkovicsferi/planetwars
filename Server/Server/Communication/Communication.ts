///<reference path='..\..\PlanetWarsServer.ts' />

module PlanetWars.Server.Communication {
    export var messages = {
        connection: 'connection',
        disconnect: 'disconnect',
        createNewPlayer: 'p',
        newPlayerCreated: 'P',
        startMove: 'm',
        endMove: 'M',
        startFire: 'f',
        endFire: 'F',
        refreshRequest: 'r',
        refresh: 'r',
        planetAddedName: 'a',
        planetRemovedName: 'A'
    }

    export var directions = {
        up: 'u',
        down: 'd',
        left: 'l',
        right: 'r'
    }
}