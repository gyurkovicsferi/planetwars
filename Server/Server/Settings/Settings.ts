///<reference path='..\..\PlanetWarsServer.ts' />

module PlanetWars.Server.Settings {
    export var WebServer = {
        port: 8080,
        startPage: '/index.html'
    }

    export var Communication = {
        playerStorageName: 'player'
    }
}