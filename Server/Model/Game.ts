///<reference path='..\PlanetWarsServer.ts' />

module PlanetWars.Model {
    export class Game {
        private lastId: number = 0;

        gravitableItems: Interfaces.IMovingGameItem[];

        planets: Planet[];
        projectiles: Projectile[];
        players: Player[];
        groups: Group[];

        constructor () {
            this.gravitableItems = [];
            this.planets = [];
            this.projectiles = [];
            this.players = [];
            this.groups = [];
        }

        public generateNewId() {
            var id = this.lastId + 1;
            this.lastId = id;
            return id;
        }
    }
}