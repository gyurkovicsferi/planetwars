///<reference path='..\PlanetWarsServer.ts' />

module PlanetWars.Model {
    export class Planet implements Interfaces.IGameItem {
        id: number;
        type: string;
        position: Utils.Vector;

        radius: number;
        gravity: number;
        positionID: number;

        constructor (id: number, position: Utils.Vector, positionID: number, radius: number, gravity: number) {
            this.id = id;
            this.position = position;

            this.type = settings.planet.typeString;
            this.positionID = positionID;
            this.radius = radius;
            this.gravity = gravity;
        }

        static isPlanet(item: Interfaces.IIdentifiable): boolean {
            return item.type === settings.planet.typeString;
        }

        toDto(): any {
            return {
                t: this.type,
                x: Math.round(this.position.x),
                y: Math.round(this.position.y),
                r: Math.round(this.radius)
            }
        }
    }
}
