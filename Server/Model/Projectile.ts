///<reference path='..\PlanetWarsServer.ts' />

module PlanetWars.Model {
    export class Projectile implements Interfaces.IMovingGameItem {
        id: number;
        type: string;
        position: Utils.Vector;
        speed: Utils.Vector;
        moveSpeed: Utils.Vector;
        
        owner: Player;
        lifeTime: number;
        radius: number;
        angle: number;

        constructor (id: number, owner: Player, position: Utils.Vector, radius: number, moveSpeed: Utils.Vector, lifeTime: number) {
            this.id = id;
            this.type = settings.projectile.typeString,
            this.position = position;
            this.speed = new Utils.Vector();
            this.moveSpeed = moveSpeed;

            this.owner = owner;
            this.lifeTime = lifeTime;
            this.radius = radius;
        }
        
        static isProjectile(item: Interfaces.IIdentifiable): boolean {
            return item.type === settings.projectile.typeString;
        }

        toDto(player: Player): any {
            return {
                t: this.type,
                x: Math.round(player.position.x - this.position.x),
                y: Math.round(player.position.y - this.position.y),
                a: Math.round(this.angle * 180 / Math.PI)
            }
        }
    }
}