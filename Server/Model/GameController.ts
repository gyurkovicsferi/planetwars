///<reference path='..\PlanetWarsServer.ts' />

module PlanetWars.Model {
    export class GameController {
        constructor (private game: Game) {
        }

       createNewPlayer(name: string, group: number): Player {
            return CreateNewPlayerRule.execute(name, group, this.game);
        }

        removePlayer(player: PlanetWars.Model.Player) {
            RemovePlayerRule.execute(player, this.game);
        }
    }
}