///<reference path='..\PlanetWarsServer.ts' />

module PlanetWars.Model {
    export class ModelController {
        public playerController: PlayerController;
        public gameController: GameController;
        public modelQueries: ModelQueries;
    }
}