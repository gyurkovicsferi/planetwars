///<reference path='..\PlanetWarsServer.ts' />

module PlanetWars.Model {
    export class Simulator {
        private previousTime: number;
        private elapsedTime: number;
        private time: number;
        private game: Game;

        constructor (game: Game) {
            this.game = game;
        }

        public startSimulate() {
            this.previousTime = Utils.Timer.getTime();
            
            setInterval(() => {
                this.simulate();
            }, settings.game.refreshRate);
        }

        private simulate() {
            this.elapsedTime = this.getElapsedTime();

            this.calculatePlayersMovement();
            this.fireProjectiles();
            this.gravitateItems();
            this.holdItems();
            this.moveItems();
            this.ageProjectiles();
        }

        private getElapsedTime(): number {
            var time: number = Utils.Timer.getTime(),
            elapsedTime: number = Utils.Timer.getElapsedTimeInSec(time, this.previousTime);

            this.previousTime = time;
            this.time = time;
            return elapsedTime;
        }

        private calculatePlayersMovement(): void {
            CalculatePlayersMovementRule.execute(this.game);
        }

        private fireProjectiles(): void {
            FireProjectilesRule.execute(this.time, this.game);
        }

        private gravitateItems(): void {
            PlanetsGravitateRule.execute(this.elapsedTime, this.game);
        }

        private holdItems(): void {
            PlanetHoldsItemsRule.execute(this.elapsedTime, this.game);
        }

        private moveItems(): void {
            MovePlayersRule.execute(this.elapsedTime, this.game);

            MoveProjectilesRule.execute(this.elapsedTime, this.game);
        }

        private ageProjectiles(): void {
            ProjectilesAgingRule.execute(this.elapsedTime, this.game);
        }
    }
}