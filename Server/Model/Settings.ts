///<reference path='..\PlanetWarsServer.ts' />

module PlanetWars.Model {
    export var settings = {
        game: {
            refreshRate: 30,
            pointsOnShot: 1,
            minusPointsOnDeath: 10,
            initialPoints: 0
        },

        player: {
            typeString: 'y',
            maxHP: 10,
            jumpSpeed: 400,
            moveSpeed: 300,
            fireTime: 150,
            defaultSize: new Utils.Size(60, 42),
            timeBetweenTeleports: 2000,
            InvulnerabilityTimeoutAfterConnect: 3000,
            InvulnerabilityTimeoutAfterShot: 300,
            InvulnerabilityTimeoutAfterDeath: 2000
        },

        planet: {
            typeString: 'n'
        },

        projectile: {
            typeString: 'j',
            demage: 1
        }
    }
}