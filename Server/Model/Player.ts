///<reference path='..\PlanetWarsServer.ts' />

module PlanetWars.Model {
    export class Player implements Interfaces.IMovingGameItem {
        id: number;
        type: string;
        position: Utils.Vector;
        speed: Utils.Vector;
        moveSpeed: Utils.Vector;

        hp: number;
        points: number;
        planet: Planet;
        canChangePlanet: boolean;
        name: string;

        isInJump: boolean;
        jumpSpeed: Utils.Vector;
        isInvulnerable: boolean;
        group: Group;
        lastFireTime: number;

        keyMap: KeyMap;
        fireAngle: number;

        size: Utils.Size = settings.player.defaultSize;

        constructor (name: string, group: Group, id: number, position: Utils.Vector, planet: Planet) {
            this.id = id;
            this.type = settings.player.typeString,
            this.position = position;
            this.speed = new Utils.Vector();
            this.moveSpeed = new Utils.Vector();
            this.jumpSpeed = new Utils.Vector();
        
            this.name = name;
            this.group = group;

            this.hp = settings.player.maxHP;
            this.points = settings.game.initialPoints;
            this.planet = planet;

            this.canChangePlanet = true;
            this.isInvulnerable = false;
            this.lastFireTime = 0;
            this.keyMap = new KeyMap();
            this.fireAngle = 0;

            this.size = settings.player.defaultSize;
        }

        getUp() {
            return Utils.Vector.vectorBetween(this.planet.position, this.position).normalize();
        }

        toDto(player: Player): any {
            return {
                t: this.type,
                n: this.name,
                p: this.points,
                x: Math.round(player.position.x - this.position.x),
                y: Math.round(player.position.y - this.position.y),
                a: Math.round(Utils.DegRadConverter.ConvertToDeg(this.getUp().getAngleFromVerticalUp())),
                h: this.hp,
                i: this.isInvulnerable,
                g: this.group.id
            }
        }

        static isPlayer(item: Interfaces.IIdentifiable): boolean {
            return item.type === settings.player.typeString;
        }
    }
}