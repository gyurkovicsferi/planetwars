///<reference path='..\PlanetWarsServer.ts' />

module PlanetWars.Model {
    export class Group implements Interfaces.IIdentifiable {
        id: number;
        type = "Group";

        players: Player[];
        score: number;

        constructor (id: number) {
            this.id = id;
            this.players = [];
            this.score = 0;
        }
    }
}