///<reference path='..\PlanetWarsServer.ts' />

module PlanetWars.Model {
    export class GameFactory {
        static newGame(): ModelController {
            var game = new Game();
            var simulator = new Simulator(game);
            var modelController = new ModelController();

            modelController.playerController = new PlayerController();
            modelController.gameController = new GameController(game);
            modelController.modelQueries = new ModelQueries(game);

            InitGameRule.execute(game);
            simulator.startSimulate();

            return modelController;
        }
    }
}