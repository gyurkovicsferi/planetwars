///<reference path='..\PlanetWarsServer.ts' />

module PlanetWars.Model {

    export class IIdentifiableUtils {
        public static getItemIndexById(id: number, array: Interfaces.IIdentifiable[]) {
            for (var i = array.length - 1; i >= 0; i--) {
                if (array[i].id === id) {
                    return i;
                }
            };
        }

        public static getItemById(id: number, array: Interfaces.IIdentifiable[]) {
            var index = this.getItemIndexById(id, array);
            return array[index];
        }

        public static removeFromArrayById(id: number, array: Interfaces.IIdentifiable[]) {
            var index = this.getItemIndexById(id, array);
            array.splice(index, 1);
        }
    }
}