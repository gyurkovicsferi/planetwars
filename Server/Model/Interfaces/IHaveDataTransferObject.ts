///<reference path='..\..\PlanetWarsServer.ts'/> 

module PlanetWars.Model.Interfaces {
    export interface IHaveDataTransferObject {
        toDto(player: Model.Player): any;
    }
}