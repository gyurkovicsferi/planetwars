///<reference path='..\..\PlanetWarsServer.ts'/>

module PlanetWars.Model.Interfaces {
    export interface IIdentifiable {
        id: number;
        type: string;
    }
}