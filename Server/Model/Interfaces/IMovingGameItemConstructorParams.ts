///<reference path='..\..\PlanetWarsServer.ts'/>

module PlanetWars.Model.Interfaces {
    export interface IMovingGameItemConstructorParams extends IGameItemConstructorParams {
        speed?: Utils.Vector;
        moveSpeed?: Utils.Vector;
    }
}