///<reference path='..\..\PlanetWarsServer.ts'/>

module PlanetWars.Model.Interfaces {
    export interface IMovingGameItem extends IGameItem {
        speed: Utils.Vector;
        moveSpeed: Utils.Vector;
    }
}