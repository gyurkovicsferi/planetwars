///<reference path='..\..\PlanetWarsServer.ts'/>

module PlanetWars.Model.Interfaces {
    export interface IGameItemConstructorParams {
        id: number;
        type: string;
        position: Utils.Vector;
    }
}