///<reference path='..\..\PlanetWarsServer.ts'/>

module PlanetWars.Model.Interfaces {
    export interface IGameItem extends IIdentifiable, IHaveDataTransferObject {
        position: Utils.Vector;
    }
}