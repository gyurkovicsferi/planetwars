///<reference path='..\PlanetWarsServer.ts' />

module PlanetWars.Model {
    export class ModelQueries {
        constructor (private game: Game) { }

        getPlanetsDto(): any[] {
            var planets = this.game.planets;
            var planetDtos = [];

            for (var i = planets.length - 1; i >= 0; i--) {
                planetDtos.push(planets[i].toDto());
            }

            return planetDtos;
        }

        getScoreForGroup(id: number): number {
            return this.game.groups[id].score;
        }

        getDtosForPlayer(player: Player): any[] {
            var itemsDTO = [];
            var players = this.game.players;
            var projectiles = this.game.projectiles;
            var item: Interfaces.IHaveDataTransferObject;
            var i: number;

            for (i = projectiles.length - 1; i >= 0; i--) {
                item = projectiles[i];
                itemsDTO.push(item.toDto(player));
            };
            for (i = players.length - 1; i >= 0; i--) {
                item = players[i];
                itemsDTO.push(item.toDto(player));
            };

            return itemsDTO;
        }
    }
}