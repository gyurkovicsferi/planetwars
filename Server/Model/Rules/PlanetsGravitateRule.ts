
///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {
    export class PlanetsGravitateRule {
        public static execute(time: number, game: Game) {
            var planets: Planet[] = game.planets,
            gravitableItems: Interfaces.IMovingGameItem[] = game.gravitableItems;
            for (var i: number = planets.length - 1; i >= 0; i--) {
                var planet = planets[i];

                for (var j: number = gravitableItems.length - 1; j >= 0; j--) {
                    var gravitableItem = gravitableItems[j];

                    this.gravitate(planet, gravitableItem, time);
                }
            }
        }

        private static gravitate(planet: Planet, item: Interfaces.IMovingGameItem, elapsedTime: number) {
            var distance = planet.position.getDistanceFrom(item.position);
            if (distance > planet.radius && distance < planet.radius * 2) {
                var distanceFromSurface = distance - planet.radius,
                    gravityFactor = -(distanceFromSurface * (planet.gravity / planet.radius)) + planet.gravity,
                    gravityAcceleration = new Utils.Vector(
                        ((planet.position.x - item.position.x) / distance) * gravityFactor,
                        ((planet.position.y - item.position.y) / distance) * gravityFactor);

                item.speed = item.speed.add(gravityAcceleration.mul(elapsedTime * 1000 / settings.game.refreshRate));
            };
        }
    }
}