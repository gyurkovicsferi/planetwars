///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {

    export class GetPlayerDestinationRule {
        public static execute(player: Player, elapsedTime: number) {
             var _speed = player.speed.add(player.jumpSpeed),
                _position = player.position.add(_speed.add(player.moveSpeed).mul(elapsedTime));

            return _position;
        }
    }
}