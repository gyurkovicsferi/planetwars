///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {
    export class ChangePlanetRule {
        public isPlanetChanged: boolean = false;

        constructor (private player: Player, private game: Game) { }

        public execute() {
            var destination: Planet;
            if (this.player.canChangePlanet) {
                destination = this.getPlanetAhead();

                if (destination !== null) {
                    this.changePlanet(destination, this.player);
                }
            }
        }

        private changePlanet(destination: Planet, player: Player): void {
            var vector = player.getUp().normalize().mul(-destination.radius);
            var position = destination.position.add(vector);

            player.position = position;
            player.planet = destination;
            this.onChangePlanet();

            this.isPlanetChanged = true;
        }

        private getPlanetAhead(): Planet {
            var up = this.player.getUp().normalize();
            var planet = this.player.planet;

            for (var i = this.game.planets.length - 1; i >= 0; i--) {
                var destinationPlanet = this.game.planets[i];
                var destinationVector = Utils.Vector.vectorBetween(planet.position, destinationPlanet.position);
                var destinationDistance = destinationVector.length();

                destinationVector = destinationVector.normalize();
                if (destinationDistance < 500) {
                    var difference = Utils.Vector.getAngleBetween(destinationVector, up) * 180 / Math.PI;
                    if (Math.abs(difference) < 20) {
                        return destinationPlanet;
                    }
                }
            }

            return null;
        }

        private onChangePlanet() {
            this.player.canChangePlanet = false;
            setTimeout(() => {
                this.player.canChangePlanet = true
            }, settings.player.timeBetweenTeleports);
        }
    }
}