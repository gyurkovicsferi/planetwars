///<reference path='..\..\PlanetWarsServer.ts' />

module PlanetWars.Model {
    export class CreateNewPlayerRule {
        public static execute(name: string, group: number, game: Game) {
            return this.addNewPlayer(name, group, game);
        }

        private static addNewPlayer(name: string, groupId: number, game: Game) {
            var planet = CreateNewPlanetRule.execute(game);

            var id = game.generateNewId(),
                position = this.generateNewPositionForPlayer(planet),
                group = <Group>IIdentifiableUtils.getItemById(groupId, game.groups),
                newPlayer = new Player(name, group, id, position, planet);

            game.players.push(newPlayer);
            game.gravitableItems.push(newPlayer);
            group.players.push(newPlayer);

            PlayersBecomeInvulnerableAfterConnect.execute(newPlayer);
            return newPlayer;
        }

        private static generateNewPositionForPlayer(planet: Planet) {
            var x = planet.position.x,
                y = planet.position.y + planet.radius;

            return new Utils.Vector(x, y);
        }
    }
}