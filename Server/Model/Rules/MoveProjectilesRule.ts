///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {
    export class MoveProjectilesRule {
        public static execute(elapsedTime: number, game: Game) {
            var items: Projectile[] = game.projectiles;
            for (var i: number = items.length - 1; i >= 0; i--) {
                var item = items[i];
                this.move(item, elapsedTime);

                ProjectilesCanHitPlayersRule.execute(item, elapsedTime, game);
            };
        }

        private static move(projectile: Projectile, elapsedTime: number) {
            var prevPos = projectile.position;
            projectile.position = projectile.position.add(projectile.speed.add(projectile.moveSpeed).mul(elapsedTime));

            var movingVector = Utils.Vector.vectorBetween(prevPos, projectile.position).normalize();
            projectile.angle = movingVector.getAngleFromVerticalUp();
        }


    }
}