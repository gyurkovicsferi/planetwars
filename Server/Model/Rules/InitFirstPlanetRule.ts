///<reference path='..\..\PlanetWarsServer.ts' />

module PlanetWars.Model {
    export class InitFirstPlanetRule {
        public static execute(game: Game) {
            var planet = new Planet(0, new Utils.Vector(0, 0), 0, 100, 100);

            game.planets.push(planet);
        }
    }
}