///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {
    export class ProjectilesCanHitPlayersRule {

        public static execute(projectile: Projectile, elapsedTime: number, game: Game) {
            this.checkCollision(projectile, elapsedTime, game);
        }

        private static checkCollision(projectile: Projectile, elapsedTime: number, game: Game): void {
            var players: Player[] = game.players;

            for (var i: number = players.length - 1; i >= 0; i--) {
                var player = players[i];
                if (this.isCollide(player, projectile, elapsedTime)) {
                    this.collide(player, projectile, game);
                }
            }
        }

        private static isCollide(player: Player, item: Projectile, elapsedTime: number): boolean {
            var itemDestination: Utils.Vector = GetProjectileDestinationRule.execute(item, elapsedTime),
                itemPosition = itemDestination.add(player.position.mul(-1)),
                itemsRelativePosition = itemPosition.rotate(-Utils.DegRadConverter.ConvertToDeg(player.getUp().getAngleFromVerticalUp()));

            if ((Math.abs(itemsRelativePosition.x) < (player.size.width / 2))) {
                if (((itemsRelativePosition.y < 0) && (itemsRelativePosition.y >= -player.size.height))) {
                    return true;
                }
            } else {
                return false;
            }
        }


        private static collide(player: Player, projectile: Projectile, game: Game) {
            if (!player.isInvulnerable) {
                this.hit(player);

                projectile.owner.points += settings.game.pointsOnShot;
                projectile.owner.group.score += settings.game.pointsOnShot;

                RemoveProjectileRule.execute(projectile, game);
            }
        }

        private static hit(player: Player) {
            player.hp -= settings.projectile.demage;
            if (player.hp === 0) {
                this.dead(player);
            } else {
                this.shot(player);
            }
        }

        private static shot(player: Player) {
            PlayersBecomeInvulnerableAfterShot.execute(player);
        }

        private static dead(player: Player) {
            PlayersBecomeInvulnerableAfterDeath.execute(player);

            player.points -= settings.game.minusPointsOnDeath;
            player.group.score -= settings.game.minusPointsOnDeath;
            player.hp = settings.player.maxHP;
        }
    }
}