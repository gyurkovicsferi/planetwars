///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {
    export class RemovePlayerRule {
        public static execute(player: Model.Player, game: Game) {
            this.removePlayerFromGroup(player, player.group);
            this.removePlayer(player, game);
            RemovePlanetRule.execute(game);
        }

        private static removePlayer(player: Model.Player, game: Game) {
            var id = player.id;

            IIdentifiableUtils.removeFromArrayById(id, game.gravitableItems);
            IIdentifiableUtils.removeFromArrayById(id, game.players);
        }

        private static removePlayerFromGroup(player: Player, group: Group) {
            var i = group.players.indexOf(player);
            group.players.splice(i, 1);
        }
    }
}