///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {
    export class InitGroupsRule {
        public static execute(game: Game) {
             var redGroup = new Group(0),
                blueGroup = new Group(1);

            this.addGroup(redGroup, game);
            this.addGroup(blueGroup, game);
        }

        private static addGroup(group: Group, game: Game) {
            game.groups.push(group);
        }
    }
}