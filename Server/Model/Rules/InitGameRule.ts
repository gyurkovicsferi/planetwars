///<reference path='..\..\PlanetWarsServer.ts' />

module PlanetWars.Model {
    export class InitGameRule {
        public static execute(game: Game) {
            this.InitGame(game);
        }

        private static InitGame(game: Game) {
            this.InitFirstPlanet(game);
            this.InitGroups(game);
        }

        private static InitFirstPlanet(game: Game) {
            InitFirstPlanetRule.execute(game);
        }

        private static InitGroups(game: Game) {
            InitGroupsRule.execute(game);
        }
    }
}