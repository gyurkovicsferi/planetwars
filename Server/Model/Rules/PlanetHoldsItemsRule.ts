///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {
    export class PlanetHoldsItemsRule {
        public static execute(time: number, game: Game) {
            var planets: Planet[] = game.planets,
            gravitableItems: Interfaces.IMovingGameItem[] = game.gravitableItems;
            for (var i: number = planets.length - 1; i >= 0; i--) {
                var planet = planets[i];

                for (var j: number = gravitableItems.length - 1; j >= 0; j--) {
                    var gravitableItem = gravitableItems[j];

                    this.hold(planet, gravitableItem, time, game);
                }
            }
        }

        private static hold(planet: Planet, item: Interfaces.IMovingGameItem, elapsedTime: number, game: Game) {
            var itemPosition = this.getDestination(item, elapsedTime),
                distance = planet.position.getDistanceFrom(itemPosition),
                distanceFromSurface = distance - planet.radius;


            if (distance <= planet.radius) {
                var newPosition = this.getLineIntersection(planet, itemPosition, item.speed.add(item.moveSpeed));
                if (newPosition !== undefined) {
                    item.position = newPosition;
                    item.speed = new Utils.Vector();

                    ItemsCollidesWithPlanetRule.execute(item, planet, game);
                }
            }
        }

        private static getDestination(item, elapsedTime): Utils.Vector {
            if (Player.isPlayer(item)) {
                return GetPlayerDestinationRule.execute(item, elapsedTime);
            }
            if (Projectile.isProjectile(item)) {
                return GetProjectileDestinationRule.execute(item, elapsedTime);
            }
        }

        private static getLineIntersection(planet: Planet, p0: Utils.Vector, v: Utils.Vector) {
            var r = planet.radius,
                p1 = Utils.Vector.vectorBetween(planet.position, p0),
                p2 = p1.add(v),
                dx = p2.x - p1.x,
                dy = p2.y - p1.y,
                dr = Math.sqrt(dx * dx + dy * dy),
                D = p1.x * p2.y - p2.x * p1.y,
                inc = r * r * dr * dr - (D * D),
                x, y;

            if (inc > 0) {
                x = (D * dy + sgn(dy) * dx * Math.sqrt(inc)) / (dr * dr);
                y = (-D * dx + Math.abs(dy) * Math.sqrt(inc)) / (dr * dr);

                var incPoint1 = new Utils.Vector(x, y);

                x = (D * dy - sgn(dy) * dx * Math.sqrt(inc)) / (dr * dr);
                y = (-D * dx - Math.abs(dy) * Math.sqrt(inc)) / (dr * dr);

                var incPoint2 = new Utils.Vector(x, y);

                var selectedIncPoint = selectBetweenIncPoints(incPoint1, incPoint2, p1);
                return planet.position.add(selectedIncPoint);
            }

            function sgn(x: number) {
                if (x < 0) return -1;
                return 1;
            }

            function selectBetweenIncPoints(incPoint1: Utils.Vector, incPoint2: Utils.Vector, refPoint: Utils.Vector) {
                var d1 = incPoint1.getDistanceFrom(refPoint),
                    d2 = incPoint2.getDistanceFrom(refPoint);

                if (d1 < d2) return incPoint1;
                return incPoint2;
            }
        }
    }
}