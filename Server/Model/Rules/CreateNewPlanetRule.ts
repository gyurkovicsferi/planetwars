///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {
    export class CreateNewPlanetRule {

        public static execute(game: Game): Model.Planet {
            var planetPosition = this.getMinimumPositionID(game),
                 planet = this.generatePlanet(planetPosition);

            this.addPlanet(planet, game);
            return planet;
        }

        private static getMinimumPositionID(game: Game): number {
            var id: number = -1;
            var i: number = 0;
            var planets = game.planets;
            var planet: Planet;
            var j;

            while (id < 0) {
                for (j = 0; j < planets.length; j++) {
                    planet = planets[j];
                    if (i === planet.positionID) {
                        break;
                    }
                }

                if (i !== planet.positionID) {
                    id = i;
                }

                i += 1;
            }
            return id;
        }

        private static generatePlanet(_positionID: number): Planet {
            var layerWidth = 350;
            var size = Math.random() * 50 + 50; //50 - 100
            var gravity: number = size * 2 + 50;

            var positionID = _positionID - 1;
            console.log("positionID: " + _positionID);
            var layersNum: number = this.getLayersNum(_positionID);
            console.log("layersNum: " + layersNum);
            var planetsInPrevLayers = this.getPlanetsInPrevLayers(_positionID);
            console.log("planetsInPrevLayers: " + planetsInPrevLayers);
                
            var positionInLayer: number = positionID - planetsInPrevLayers;
            console.log("positionInLayer: " + positionInLayer);

            var angle: number = 60 / layersNum;
            console.log("angle: " + angle);
            var planet: Planet;
            var position: Utils.Vector;

            if (_positionID === 0) {
                position = new Utils.Vector(0, 0);
            }
            else {
                position = new Utils.Vector(0, -1).mul(layerWidth).mul(layersNum).rotate(positionInLayer * angle);
            }

            planet = new Planet(_positionID, position, _positionID, size, gravity);

            return planet;
        }
        
        private static getLayersNum(positionID: number): number {
            var layersNum = 0;
            var layersPlanet = 0;

            while (positionID > 0) {              
                layersPlanet += 6;
                positionID -= layersPlanet;                                   
                layersNum += 1;
             }
            return layersNum;
        }

        private static getPlanetsInPrevLayers(positionID: number): number {
            
            var layersPlanet = 0;
            var prevLayersPlanets = 0;
            
            while (positionID > 0) {              
                layersPlanet += 6;
                prevLayersPlanets += layersPlanet;
                positionID -= layersPlanet;      
             }

            return prevLayersPlanets - layersPlanet;
        }

        private static addPlanet(planet: Planet, game: Game) {
            game.planets.push(planet);
            Server.GameServer.publishPlanetAdded(planet);
        }
    }
}