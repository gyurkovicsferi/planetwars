///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {
    export class GetProjectileDestinationRule {
        public static execute(projectile: Projectile, elapsedTime: number) {
            var speed = projectile.speed,
                position = projectile.position.add(projectile.speed.add(projectile.moveSpeed).mul(elapsedTime));

            return position;
        }
    }
}