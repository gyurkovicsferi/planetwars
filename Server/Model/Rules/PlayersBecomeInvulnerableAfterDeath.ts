///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {
    export class PlayersBecomeInvulnerableAfterDeath {
        static execute(player: Player) {
            this.startInvulnerability(player);
        }

        private static startInvulnerability(player: Player) {
            player.isInvulnerable = true;
            setTimeout(() => PlayersBecomeInvulnerableAfterDeath.endInvulnerability(player), settings.player.InvulnerabilityTimeoutAfterDeath);
        }

        private static endInvulnerability(player: Player) {
            player.isInvulnerable = false;
        }
    }
}