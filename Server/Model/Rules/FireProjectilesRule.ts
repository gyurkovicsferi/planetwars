
///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {
    export class FireProjectilesRule {
        public static execute(time: number, game: Game) {
            var players: Player[] = game.players;

            for (var i: number = players.length - 1; i >= 0; i--) {
                var player = players[i];
                if (player.keyMap.fire) {
                    if (this.canFire(player, time)) {
                        this.fire(player, time);
                        this.addNewProjectile(player, game);
                    }
                }
            };
        }

        private static canFire(player: Player, time: number): boolean {
            return time - player.lastFireTime > settings.player.fireTime;
        }

        private static fire(player: Player, time: number) {
            player.lastFireTime = time;
        }

        private static addNewProjectile(player: Player, game: Game) {
            var id = game.generateNewId(),
                angle = player.fireAngle,
                startVector = new Utils.Vector(0, -1).rotate(angle),
                position = this.generateNewPositionForProjectile(player, startVector),
                newProjectile = new Projectile(id, player, position, 2, startVector.mul(1000), 2);

            game.gravitableItems.push(newProjectile);
            game.projectiles.push(newProjectile);

            return newProjectile;
        }

        private static generateNewPositionForProjectile(player: Player, startVector: Utils.Vector) {
            var position = player.position.add(player.getUp().mul(30)),
                x = position.x,
                y = position.y;

            return new Utils.Vector(x, y).add(startVector.mul(30));
        }
    }
}