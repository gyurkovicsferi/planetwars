///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {
    export class MovePlayersRule {
        public static execute(time: number, game: Game) {
            var players: Player[] = game.players;
            for (var i: number = players.length - 1; i >= 0; i--) {
                var player = players[i];
                this.move(player, time);
            };
        }

        private static move(player: Player, elapsedTime: number) {
            player.speed = player.speed.add(player.jumpSpeed);
            player.position = player.position.add(player.speed.add(player.moveSpeed).mul(elapsedTime));

            player.jumpSpeed = new Utils.Vector();
            player.moveSpeed = new Utils.Vector();
        }
    }
}