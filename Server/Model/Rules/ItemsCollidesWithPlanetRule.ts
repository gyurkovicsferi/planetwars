///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {
    export class ItemsCollidesWithPlanetRule {
        public static execute(item: Interfaces.IMovingGameItem, planet: Planet, game: Game) {
            if (Player.isPlayer(item)) {
                this.playerCollidesWithPlanet(<Player>item);
            }

            if (Projectile.isProjectile(item)) {
                this.projectileCollidesWithPlanet(<Projectile>item, game);
            }
        }

        private static playerCollidesWithPlanet(player: Player) {
            player.isInJump = false;
            player.jumpSpeed = new Utils.Vector();
        }

        private static projectileCollidesWithPlanet(projectile: Projectile, game: Game) {
            RemoveProjectileRule.execute(projectile, game);
        }
    }
}