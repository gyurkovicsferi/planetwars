///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {
    export class CalculatePlayersMovementRule {
        public static execute(game: Game) {
            var players: Player[] = game.players;

            for (var i: number = players.length - 1; i >= 0; i--) {
                var player = players[i];

                if (player.keyMap.left) this.left(player);
                if (player.keyMap.right) this.right(player);

                if (player.keyMap.up) {
                    this.jumpOrChangePlanet(player, game);
                }
            };
        }

        private static left(player: Player) {
            var left = player.getUp().rotateLeft();

            player.moveSpeed = player.moveSpeed.add(left.mul(settings.player.moveSpeed));
        }

        private static right(player: Player) {
            var right = player.getUp().rotateRight();

            player.moveSpeed = player.moveSpeed.add(right.mul(settings.player.moveSpeed));
        }

        private static jump(player: Player) {
            var up = player.getUp();
            if (!player.isInJump) {
                player.isInJump = true;
                player.jumpSpeed = player.jumpSpeed.add(up.mul(settings.player.jumpSpeed));
            }
        }

        private static jumpOrChangePlanet(player: Player, game: Game) {
            var changePlanetRule = new ChangePlanetRule(player, game);
            changePlanetRule.execute();

            if (!changePlanetRule.isPlanetChanged) {
                this.jump(player);
            };
        }


    }
}