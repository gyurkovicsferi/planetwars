///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {
    export class PlayersBecomeInvulnerableAfterConnect {
        static execute(player: Player) {
            this.startInvulnerability(player);
        }

        private static startInvulnerability(player: Player) {
            player.isInvulnerable = true;
            setTimeout(() => PlayersBecomeInvulnerableAfterConnect.endInvulnerability(player), settings.player.InvulnerabilityTimeoutAfterConnect);
        }

        private static endInvulnerability(player: Player) {
            player.isInvulnerable = false;
        }
    }
}