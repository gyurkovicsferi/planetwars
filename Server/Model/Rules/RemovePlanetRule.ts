///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {

    export class RemovePlanetRule {
        public static execute(game: Game) {
            var id = this.getMaximumPositionIDWithoutPlayer(game);
            var planet = <Planet>IIdentifiableUtils.getItemById(id, game.planets);
            IIdentifiableUtils.removeFromArrayById(id, game.planets);

            Server.GameServer.publishPlanetRemoved(planet);
        }


        private static getMaximumPositionIDWithoutPlayer(game: Game): number {
            var id: number = -1,
            i: number = 0,
            max: number,
                planets = game.planets,
                players = game.players,
            planet: Planet,
            player: Player,
            j: number,
            planetsWithoutPlayers: Planet[] = [];

            for (j = 0; j < planets.length; j++) {
                planet = planets[j];

                for (i = 0; i < players.length; i++) {
                    player = players[i];

                    if (player.planet === planet) {
                        break;
                    }
                }

                if (typeof player !== "undefined") {
                    if (player.planet !== planet) {
                        planetsWithoutPlayers.push(planet);
                    }
                }
            }

            max = -1;
            id = -1;
            for (i = 0; i < planetsWithoutPlayers.length; i++) {
                planet = planetsWithoutPlayers[i];

                if (planet.positionID > max) {
                    max = planet.positionID;
                    id = planet.id;
                }
            }

            return id;
        }
    }
}