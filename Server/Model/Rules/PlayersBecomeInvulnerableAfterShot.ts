///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {

    export class PlayersBecomeInvulnerableAfterShot {
        static execute(player: Player) {
            this.startInvulnerability(player);
        }

        private static startInvulnerability(player: Player) {
            player.isInvulnerable = true;

            setTimeout(() => PlayersBecomeInvulnerableAfterShot.endInvulnerability(player), settings.player.InvulnerabilityTimeoutAfterShot);
        }

        private static endInvulnerability(player: Player) {
            player.isInvulnerable = false;
        }
    }
}