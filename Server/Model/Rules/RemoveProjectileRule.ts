///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {
    export class RemoveProjectileRule {
        static execute(projectile: Projectile, game: Game) {
            IIdentifiableUtils.removeFromArrayById(projectile.id, game.gravitableItems);
            IIdentifiableUtils.removeFromArrayById(projectile.id, game.projectiles);
        }
    }
}