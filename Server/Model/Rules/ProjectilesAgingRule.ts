///<reference path='..\..\PlanetWarsServer.ts' />


module PlanetWars.Model {
    export class ProjectilesAgingRule {
        public static execute(time: number, game: Game) {
            var items: Projectile[] = game.projectiles;
            for (var i: number = items.length - 1; i >= 0; i--) {
                var item = items[i];
                this.ageProjectile(item, time, game);
            };
        }

        private static ageProjectile(projectile: Projectile, elapsedTime: number, game: Game) {
            if (projectile.lifeTime < 0) {
                RemoveProjectileRule.execute(projectile, game);
            } else {
                projectile.lifeTime -= elapsedTime;
            }
        }
    }
}