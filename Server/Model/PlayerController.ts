///<reference path='..\PlanetWarsServer.ts' />

module PlanetWars.Model {

    export class PlayerController {
        startMoveUp(player: Model.Player) {
            player.keyMap.up = true;
        }

        startMoveLeft(player: Model.Player) {
            player.keyMap.left = true;
        }

        startMoveRight(player: Model.Player) {
            player.keyMap.right = true;
        }

        endMoveUp(player: Model.Player) {
            player.keyMap.up = false;
        }

        endMoveLeft(player: Model.Player) {
            player.keyMap.left = false;
        }

        endMoveRight(player: Model.Player) {
            player.keyMap.right = false;
        }

        startFire(player: Model.Player) {
            player.keyMap.fire = true;
        }

        endFire(player: Model.Player) {
            player.keyMap.fire = false;
        }
    }
}