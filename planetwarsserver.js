///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Utils;
    (function (Utils) {
        var Timer = (function () {
            function Timer() {
            }
            Timer.getTime = function () {
                return new Date().getTime();
            };
            Timer.getElapsedTimeInSec = function (time, previousTime) {
                return (time - previousTime) / 1000;
            };
            return Timer;
        })();
        Utils.Timer = Timer;
    })(Utils = PlanetWars.Utils || (PlanetWars.Utils = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Utils;
    (function (Utils) {
        var Vector = (function () {
            function Vector(x, y) {
                if (x === void 0) { x = 0; }
                if (y === void 0) { y = 0; }
                this.x = x;
                this.y = y;
            }
            Vector.vectorBetween = function (fromVector, toVector) {
                return new Vector(toVector.x - fromVector.x, toVector.y - fromVector.y);
            };
            Vector.prototype.getDistanceFrom = function (otherVector) {
                var distanceVector = Vector.vectorBetween(this, otherVector);
                return distanceVector.length();
            };
            Vector.prototype.length = function () {
                return Math.sqrt(this.x * this.x + this.y * this.y);
            };
            Vector.prototype.add = function (vector) {
                return new Vector(this.x + vector.x, this.y + vector.y);
            };
            Vector.prototype.mul = function (num) {
                return new Vector(this.x * num, this.y * num);
            };
            Vector.prototype.normalize = function () {
                var length = this.length();
                return new Vector(this.x / length, this.y / length);
            };
            Vector.prototype.getAngleFromVerticalUp = function () {
                var factor;
                if (this.x < 0) {
                    factor = -1;
                }
                else {
                    factor = 1;
                }
                return factor * Math.acos(-this.y / this.length());
            };
            Vector.getAngleBetween = function (a, b) {
                return Math.acos(this.dotProduct(a, b) / (a.length() * b.length()));
            };
            Vector.dotProduct = function (a, b) {
                return a.x * b.x + a.y * b.y;
            };
            Vector.prototype.rotate = function (angle) {
                var angleRad = Utils.DegRadConverter.ConvertToRad(angle), sinA = Math.sin(angleRad), cosA = Math.cos(angleRad);
                return new Vector(this.x * cosA - this.y * sinA, this.x * sinA + this.y * cosA);
            };
            Vector.prototype.rotateLeft = function () {
                return new Vector(this.y, -this.x);
            };
            Vector.prototype.rotateRight = function () {
                return new Vector(-this.y, this.x);
            };
            return Vector;
        })();
        Utils.Vector = Vector;
    })(Utils = PlanetWars.Utils || (PlanetWars.Utils = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Utils;
    (function (Utils) {
        var Size = (function () {
            function Size(width, height) {
                this.width = width;
                this.height = height;
            }
            return Size;
        })();
        Utils.Size = Size;
    })(Utils = PlanetWars.Utils || (PlanetWars.Utils = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Utils;
    (function (Utils) {
        var DegRadConverter = (function () {
            function DegRadConverter() {
            }
            DegRadConverter.ConvertToRad = function (degree) {
                return degree * Math.PI / 180;
            };
            DegRadConverter.ConvertToDeg = function (radian) {
                return radian / Math.PI * 180;
            };
            return DegRadConverter;
        })();
        Utils.DegRadConverter = DegRadConverter;
    })(Utils = PlanetWars.Utils || (PlanetWars.Utils = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts'/>
///<reference path='..\..\PlanetWarsServer.ts'/>
///<reference path='..\..\PlanetWarsServer.ts'/>
///<reference path='..\..\PlanetWarsServer.ts'/> 
///<reference path='..\..\PlanetWarsServer.ts'/>
///<reference path='..\..\PlanetWarsServer.ts'/>
///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var IIdentifiableUtils = (function () {
            function IIdentifiableUtils() {
            }
            IIdentifiableUtils.getItemIndexById = function (id, array) {
                for (var i = array.length - 1; i >= 0; i--) {
                    if (array[i].id === id) {
                        return i;
                    }
                }
                ;
            };
            IIdentifiableUtils.getItemById = function (id, array) {
                var index = this.getItemIndexById(id, array);
                return array[index];
            };
            IIdentifiableUtils.removeFromArrayById = function (id, array) {
                var index = this.getItemIndexById(id, array);
                array.splice(index, 1);
            };
            return IIdentifiableUtils;
        })();
        Model.IIdentifiableUtils = IIdentifiableUtils;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var Planet = (function () {
            function Planet(id, position, positionID, radius, gravity) {
                this.id = id;
                this.position = position;
                this.type = Model.settings.planet.typeString;
                this.positionID = positionID;
                this.radius = radius;
                this.gravity = gravity;
            }
            Planet.isPlanet = function (item) {
                return item.type === Model.settings.planet.typeString;
            };
            Planet.prototype.toDto = function () {
                return {
                    t: this.type,
                    x: Math.round(this.position.x),
                    y: Math.round(this.position.y),
                    r: Math.round(this.radius)
                };
            };
            return Planet;
        })();
        Model.Planet = Planet;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var Player = (function () {
            function Player(name, group, id, position, planet) {
                this.size = Model.settings.player.defaultSize;
                this.id = id;
                this.type = Model.settings.player.typeString, this.position = position;
                this.speed = new PlanetWars.Utils.Vector();
                this.moveSpeed = new PlanetWars.Utils.Vector();
                this.jumpSpeed = new PlanetWars.Utils.Vector();
                this.name = name;
                this.group = group;
                this.hp = Model.settings.player.maxHP;
                this.points = Model.settings.game.initialPoints;
                this.planet = planet;
                this.canChangePlanet = true;
                this.isInvulnerable = false;
                this.lastFireTime = 0;
                this.keyMap = new Model.KeyMap();
                this.fireAngle = 0;
                this.size = Model.settings.player.defaultSize;
            }
            Player.prototype.getUp = function () {
                return PlanetWars.Utils.Vector.vectorBetween(this.planet.position, this.position).normalize();
            };
            Player.prototype.toDto = function (player) {
                return {
                    t: this.type,
                    n: this.name,
                    p: this.points,
                    x: Math.round(player.position.x - this.position.x),
                    y: Math.round(player.position.y - this.position.y),
                    a: Math.round(PlanetWars.Utils.DegRadConverter.ConvertToDeg(this.getUp().getAngleFromVerticalUp())),
                    h: this.hp,
                    i: this.isInvulnerable,
                    g: this.group.id
                };
            };
            Player.isPlayer = function (item) {
                return item.type === Model.settings.player.typeString;
            };
            return Player;
        })();
        Model.Player = Player;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var Projectile = (function () {
            function Projectile(id, owner, position, radius, moveSpeed, lifeTime) {
                this.id = id;
                this.type = Model.settings.projectile.typeString, this.position = position;
                this.speed = new PlanetWars.Utils.Vector();
                this.moveSpeed = moveSpeed;
                this.owner = owner;
                this.lifeTime = lifeTime;
                this.radius = radius;
            }
            Projectile.isProjectile = function (item) {
                return item.type === Model.settings.projectile.typeString;
            };
            Projectile.prototype.toDto = function (player) {
                return {
                    t: this.type,
                    x: Math.round(player.position.x - this.position.x),
                    y: Math.round(player.position.y - this.position.y),
                    a: Math.round(this.angle * 180 / Math.PI)
                };
            };
            return Projectile;
        })();
        Model.Projectile = Projectile;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var Group = (function () {
            function Group(id) {
                this.type = "Group";
                this.id = id;
                this.players = [];
                this.score = 0;
            }
            return Group;
        })();
        Model.Group = Group;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var KeyMap = (function () {
            function KeyMap() {
                this.up = false;
                this.down = false;
                this.left = false;
                this.right = false;
                this.fire = false;
            }
            return KeyMap;
        })();
        Model.KeyMap = KeyMap;
        ;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var Game = (function () {
            function Game() {
                this.lastId = 0;
                this.gravitableItems = [];
                this.planets = [];
                this.projectiles = [];
                this.players = [];
                this.groups = [];
            }
            Game.prototype.generateNewId = function () {
                var id = this.lastId + 1;
                this.lastId = id;
                return id;
            };
            return Game;
        })();
        Model.Game = Game;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var GameFactory = (function () {
            function GameFactory() {
            }
            GameFactory.newGame = function () {
                var game = new Model.Game();
                var simulator = new Model.Simulator(game);
                var modelController = new Model.ModelController();
                modelController.playerController = new Model.PlayerController();
                modelController.gameController = new Model.GameController(game);
                modelController.modelQueries = new Model.ModelQueries(game);
                Model.InitGameRule.execute(game);
                simulator.startSimulate();
                return modelController;
            };
            return GameFactory;
        })();
        Model.GameFactory = GameFactory;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var GameController = (function () {
            function GameController(game) {
                this.game = game;
            }
            GameController.prototype.createNewPlayer = function (name, group) {
                return Model.CreateNewPlayerRule.execute(name, group, this.game);
            };
            GameController.prototype.removePlayer = function (player) {
                Model.RemovePlayerRule.execute(player, this.game);
            };
            return GameController;
        })();
        Model.GameController = GameController;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var ModelController = (function () {
            function ModelController() {
            }
            return ModelController;
        })();
        Model.ModelController = ModelController;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var ModelQueries = (function () {
            function ModelQueries(game) {
                this.game = game;
            }
            ModelQueries.prototype.getPlanetsDto = function () {
                var planets = this.game.planets;
                var planetDtos = [];
                for (var i = planets.length - 1; i >= 0; i--) {
                    planetDtos.push(planets[i].toDto());
                }
                return planetDtos;
            };
            ModelQueries.prototype.getScoreForGroup = function (id) {
                return this.game.groups[id].score;
            };
            ModelQueries.prototype.getDtosForPlayer = function (player) {
                var itemsDTO = [];
                var players = this.game.players;
                var projectiles = this.game.projectiles;
                var item;
                var i;
                for (i = projectiles.length - 1; i >= 0; i--) {
                    item = projectiles[i];
                    itemsDTO.push(item.toDto(player));
                }
                ;
                for (i = players.length - 1; i >= 0; i--) {
                    item = players[i];
                    itemsDTO.push(item.toDto(player));
                }
                ;
                return itemsDTO;
            };
            return ModelQueries;
        })();
        Model.ModelQueries = ModelQueries;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var PlayerController = (function () {
            function PlayerController() {
            }
            PlayerController.prototype.startMoveUp = function (player) {
                player.keyMap.up = true;
            };
            PlayerController.prototype.startMoveLeft = function (player) {
                player.keyMap.left = true;
            };
            PlayerController.prototype.startMoveRight = function (player) {
                player.keyMap.right = true;
            };
            PlayerController.prototype.endMoveUp = function (player) {
                player.keyMap.up = false;
            };
            PlayerController.prototype.endMoveLeft = function (player) {
                player.keyMap.left = false;
            };
            PlayerController.prototype.endMoveRight = function (player) {
                player.keyMap.right = false;
            };
            PlayerController.prototype.startFire = function (player) {
                player.keyMap.fire = true;
            };
            PlayerController.prototype.endFire = function (player) {
                player.keyMap.fire = false;
            };
            return PlayerController;
        })();
        Model.PlayerController = PlayerController;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        Model.settings = {
            game: {
                refreshRate: 30,
                pointsOnShot: 1,
                minusPointsOnDeath: 10,
                initialPoints: 0
            },
            player: {
                typeString: 'y',
                maxHP: 10,
                jumpSpeed: 400,
                moveSpeed: 300,
                fireTime: 150,
                defaultSize: new PlanetWars.Utils.Size(60, 42),
                timeBetweenTeleports: 2000,
                InvulnerabilityTimeoutAfterConnect: 3000,
                InvulnerabilityTimeoutAfterShot: 300,
                InvulnerabilityTimeoutAfterDeath: 2000
            },
            planet: {
                typeString: 'n'
            },
            projectile: {
                typeString: 'j',
                demage: 1
            }
        };
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var InitGameRule = (function () {
            function InitGameRule() {
            }
            InitGameRule.execute = function (game) {
                this.InitGame(game);
            };
            InitGameRule.InitGame = function (game) {
                this.InitFirstPlanet(game);
                this.InitGroups(game);
            };
            InitGameRule.InitFirstPlanet = function (game) {
                Model.InitFirstPlanetRule.execute(game);
            };
            InitGameRule.InitGroups = function (game) {
                Model.InitGroupsRule.execute(game);
            };
            return InitGameRule;
        })();
        Model.InitGameRule = InitGameRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var InitFirstPlanetRule = (function () {
            function InitFirstPlanetRule() {
            }
            InitFirstPlanetRule.execute = function (game) {
                var planet = new Model.Planet(0, new PlanetWars.Utils.Vector(0, 0), 0, 100, 100);
                game.planets.push(planet);
            };
            return InitFirstPlanetRule;
        })();
        Model.InitFirstPlanetRule = InitFirstPlanetRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var InitGroupsRule = (function () {
            function InitGroupsRule() {
            }
            InitGroupsRule.execute = function (game) {
                var redGroup = new Model.Group(0), blueGroup = new Model.Group(1);
                this.addGroup(redGroup, game);
                this.addGroup(blueGroup, game);
            };
            InitGroupsRule.addGroup = function (group, game) {
                game.groups.push(group);
            };
            return InitGroupsRule;
        })();
        Model.InitGroupsRule = InitGroupsRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var CreateNewPlayerRule = (function () {
            function CreateNewPlayerRule() {
            }
            CreateNewPlayerRule.execute = function (name, group, game) {
                return this.addNewPlayer(name, group, game);
            };
            CreateNewPlayerRule.addNewPlayer = function (name, groupId, game) {
                var planet = Model.CreateNewPlanetRule.execute(game);
                var id = game.generateNewId(), position = this.generateNewPositionForPlayer(planet), group = Model.IIdentifiableUtils.getItemById(groupId, game.groups), newPlayer = new Model.Player(name, group, id, position, planet);
                game.players.push(newPlayer);
                game.gravitableItems.push(newPlayer);
                group.players.push(newPlayer);
                Model.PlayersBecomeInvulnerableAfterConnect.execute(newPlayer);
                return newPlayer;
            };
            CreateNewPlayerRule.generateNewPositionForPlayer = function (planet) {
                var x = planet.position.x, y = planet.position.y + planet.radius;
                return new PlanetWars.Utils.Vector(x, y);
            };
            return CreateNewPlayerRule;
        })();
        Model.CreateNewPlayerRule = CreateNewPlayerRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var CreateNewPlanetRule = (function () {
            function CreateNewPlanetRule() {
            }
            CreateNewPlanetRule.execute = function (game) {
                var planetPosition = this.getMinimumPositionID(game), planet = this.generatePlanet(planetPosition);
                this.addPlanet(planet, game);
                return planet;
            };
            CreateNewPlanetRule.getMinimumPositionID = function (game) {
                var id = -1;
                var i = 0;
                var planets = game.planets;
                var planet;
                var j;
                while (id < 0) {
                    for (j = 0; j < planets.length; j++) {
                        planet = planets[j];
                        if (i === planet.positionID) {
                            break;
                        }
                    }
                    if (i !== planet.positionID) {
                        id = i;
                    }
                    i += 1;
                }
                return id;
            };
            CreateNewPlanetRule.generatePlanet = function (_positionID) {
                var layerWidth = 350;
                var size = Math.random() * 50 + 50; //50 - 100
                var gravity = size * 2 + 50;
                var positionID = _positionID - 1;
                console.log("positionID: " + _positionID);
                var layersNum = this.getLayersNum(_positionID);
                console.log("layersNum: " + layersNum);
                var planetsInPrevLayers = this.getPlanetsInPrevLayers(_positionID);
                console.log("planetsInPrevLayers: " + planetsInPrevLayers);
                var positionInLayer = positionID - planetsInPrevLayers;
                console.log("positionInLayer: " + positionInLayer);
                var angle = 60 / layersNum;
                console.log("angle: " + angle);
                var planet;
                var position;
                if (_positionID === 0) {
                    position = new PlanetWars.Utils.Vector(0, 0);
                }
                else {
                    position = new PlanetWars.Utils.Vector(0, -1).mul(layerWidth).mul(layersNum).rotate(positionInLayer * angle);
                }
                planet = new Model.Planet(_positionID, position, _positionID, size, gravity);
                return planet;
            };
            CreateNewPlanetRule.getLayersNum = function (positionID) {
                var layersNum = 0;
                var layersPlanet = 0;
                while (positionID > 0) {
                    layersPlanet += 6;
                    positionID -= layersPlanet;
                    layersNum += 1;
                }
                return layersNum;
            };
            CreateNewPlanetRule.getPlanetsInPrevLayers = function (positionID) {
                var layersPlanet = 0;
                var prevLayersPlanets = 0;
                while (positionID > 0) {
                    layersPlanet += 6;
                    prevLayersPlanets += layersPlanet;
                    positionID -= layersPlanet;
                }
                return prevLayersPlanets - layersPlanet;
            };
            CreateNewPlanetRule.addPlanet = function (planet, game) {
                game.planets.push(planet);
                PlanetWars.Server.GameServer.publishPlanetAdded(planet);
            };
            return CreateNewPlanetRule;
        })();
        Model.CreateNewPlanetRule = CreateNewPlanetRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var RemovePlayerRule = (function () {
            function RemovePlayerRule() {
            }
            RemovePlayerRule.execute = function (player, game) {
                this.removePlayerFromGroup(player, player.group);
                this.removePlayer(player, game);
                Model.RemovePlanetRule.execute(game);
            };
            RemovePlayerRule.removePlayer = function (player, game) {
                var id = player.id;
                Model.IIdentifiableUtils.removeFromArrayById(id, game.gravitableItems);
                Model.IIdentifiableUtils.removeFromArrayById(id, game.players);
            };
            RemovePlayerRule.removePlayerFromGroup = function (player, group) {
                var i = group.players.indexOf(player);
                group.players.splice(i, 1);
            };
            return RemovePlayerRule;
        })();
        Model.RemovePlayerRule = RemovePlayerRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var RemovePlanetRule = (function () {
            function RemovePlanetRule() {
            }
            RemovePlanetRule.execute = function (game) {
                var id = this.getMaximumPositionIDWithoutPlayer(game);
                var planet = Model.IIdentifiableUtils.getItemById(id, game.planets);
                Model.IIdentifiableUtils.removeFromArrayById(id, game.planets);
                PlanetWars.Server.GameServer.publishPlanetRemoved(planet);
            };
            RemovePlanetRule.getMaximumPositionIDWithoutPlayer = function (game) {
                var id = -1, i = 0, max, planets = game.planets, players = game.players, planet, player, j, planetsWithoutPlayers = [];
                for (j = 0; j < planets.length; j++) {
                    planet = planets[j];
                    for (i = 0; i < players.length; i++) {
                        player = players[i];
                        if (player.planet === planet) {
                            break;
                        }
                    }
                    if (typeof player !== "undefined") {
                        if (player.planet !== planet) {
                            planetsWithoutPlayers.push(planet);
                        }
                    }
                }
                max = -1;
                id = -1;
                for (i = 0; i < planetsWithoutPlayers.length; i++) {
                    planet = planetsWithoutPlayers[i];
                    if (planet.positionID > max) {
                        max = planet.positionID;
                        id = planet.id;
                    }
                }
                return id;
            };
            return RemovePlanetRule;
        })();
        Model.RemovePlanetRule = RemovePlanetRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var CalculatePlayersMovementRule = (function () {
            function CalculatePlayersMovementRule() {
            }
            CalculatePlayersMovementRule.execute = function (game) {
                var players = game.players;
                for (var i = players.length - 1; i >= 0; i--) {
                    var player = players[i];
                    if (player.keyMap.left)
                        this.left(player);
                    if (player.keyMap.right)
                        this.right(player);
                    if (player.keyMap.up) {
                        this.jumpOrChangePlanet(player, game);
                    }
                }
                ;
            };
            CalculatePlayersMovementRule.left = function (player) {
                var left = player.getUp().rotateLeft();
                player.moveSpeed = player.moveSpeed.add(left.mul(Model.settings.player.moveSpeed));
            };
            CalculatePlayersMovementRule.right = function (player) {
                var right = player.getUp().rotateRight();
                player.moveSpeed = player.moveSpeed.add(right.mul(Model.settings.player.moveSpeed));
            };
            CalculatePlayersMovementRule.jump = function (player) {
                var up = player.getUp();
                if (!player.isInJump) {
                    player.isInJump = true;
                    player.jumpSpeed = player.jumpSpeed.add(up.mul(Model.settings.player.jumpSpeed));
                }
            };
            CalculatePlayersMovementRule.jumpOrChangePlanet = function (player, game) {
                var changePlanetRule = new Model.ChangePlanetRule(player, game);
                changePlanetRule.execute();
                if (!changePlanetRule.isPlanetChanged) {
                    this.jump(player);
                }
                ;
            };
            return CalculatePlayersMovementRule;
        })();
        Model.CalculatePlayersMovementRule = CalculatePlayersMovementRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var ChangePlanetRule = (function () {
            function ChangePlanetRule(player, game) {
                this.player = player;
                this.game = game;
                this.isPlanetChanged = false;
            }
            ChangePlanetRule.prototype.execute = function () {
                var destination;
                if (this.player.canChangePlanet) {
                    destination = this.getPlanetAhead();
                    if (destination !== null) {
                        this.changePlanet(destination, this.player);
                    }
                }
            };
            ChangePlanetRule.prototype.changePlanet = function (destination, player) {
                var vector = player.getUp().normalize().mul(-destination.radius);
                var position = destination.position.add(vector);
                player.position = position;
                player.planet = destination;
                this.onChangePlanet();
                this.isPlanetChanged = true;
            };
            ChangePlanetRule.prototype.getPlanetAhead = function () {
                var up = this.player.getUp().normalize();
                var planet = this.player.planet;
                for (var i = this.game.planets.length - 1; i >= 0; i--) {
                    var destinationPlanet = this.game.planets[i];
                    var destinationVector = PlanetWars.Utils.Vector.vectorBetween(planet.position, destinationPlanet.position);
                    var destinationDistance = destinationVector.length();
                    destinationVector = destinationVector.normalize();
                    if (destinationDistance < 500) {
                        var difference = PlanetWars.Utils.Vector.getAngleBetween(destinationVector, up) * 180 / Math.PI;
                        if (Math.abs(difference) < 20) {
                            return destinationPlanet;
                        }
                    }
                }
                return null;
            };
            ChangePlanetRule.prototype.onChangePlanet = function () {
                var _this = this;
                this.player.canChangePlanet = false;
                setTimeout(function () {
                    _this.player.canChangePlanet = true;
                }, Model.settings.player.timeBetweenTeleports);
            };
            return ChangePlanetRule;
        })();
        Model.ChangePlanetRule = ChangePlanetRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var FireProjectilesRule = (function () {
            function FireProjectilesRule() {
            }
            FireProjectilesRule.execute = function (time, game) {
                var players = game.players;
                for (var i = players.length - 1; i >= 0; i--) {
                    var player = players[i];
                    if (player.keyMap.fire) {
                        if (this.canFire(player, time)) {
                            this.fire(player, time);
                            this.addNewProjectile(player, game);
                        }
                    }
                }
                ;
            };
            FireProjectilesRule.canFire = function (player, time) {
                return time - player.lastFireTime > Model.settings.player.fireTime;
            };
            FireProjectilesRule.fire = function (player, time) {
                player.lastFireTime = time;
            };
            FireProjectilesRule.addNewProjectile = function (player, game) {
                var id = game.generateNewId(), angle = player.fireAngle, startVector = new PlanetWars.Utils.Vector(0, -1).rotate(angle), position = this.generateNewPositionForProjectile(player, startVector), newProjectile = new Model.Projectile(id, player, position, 2, startVector.mul(1000), 2);
                game.gravitableItems.push(newProjectile);
                game.projectiles.push(newProjectile);
                return newProjectile;
            };
            FireProjectilesRule.generateNewPositionForProjectile = function (player, startVector) {
                var position = player.position.add(player.getUp().mul(30)), x = position.x, y = position.y;
                return new PlanetWars.Utils.Vector(x, y).add(startVector.mul(30));
            };
            return FireProjectilesRule;
        })();
        Model.FireProjectilesRule = FireProjectilesRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var PlanetsGravitateRule = (function () {
            function PlanetsGravitateRule() {
            }
            PlanetsGravitateRule.execute = function (time, game) {
                var planets = game.planets, gravitableItems = game.gravitableItems;
                for (var i = planets.length - 1; i >= 0; i--) {
                    var planet = planets[i];
                    for (var j = gravitableItems.length - 1; j >= 0; j--) {
                        var gravitableItem = gravitableItems[j];
                        this.gravitate(planet, gravitableItem, time);
                    }
                }
            };
            PlanetsGravitateRule.gravitate = function (planet, item, elapsedTime) {
                var distance = planet.position.getDistanceFrom(item.position);
                if (distance > planet.radius && distance < planet.radius * 2) {
                    var distanceFromSurface = distance - planet.radius, gravityFactor = -(distanceFromSurface * (planet.gravity / planet.radius)) + planet.gravity, gravityAcceleration = new PlanetWars.Utils.Vector(((planet.position.x - item.position.x) / distance) * gravityFactor, ((planet.position.y - item.position.y) / distance) * gravityFactor);
                    item.speed = item.speed.add(gravityAcceleration.mul(elapsedTime * 1000 / Model.settings.game.refreshRate));
                }
                ;
            };
            return PlanetsGravitateRule;
        })();
        Model.PlanetsGravitateRule = PlanetsGravitateRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var PlanetHoldsItemsRule = (function () {
            function PlanetHoldsItemsRule() {
            }
            PlanetHoldsItemsRule.execute = function (time, game) {
                var planets = game.planets, gravitableItems = game.gravitableItems;
                for (var i = planets.length - 1; i >= 0; i--) {
                    var planet = planets[i];
                    for (var j = gravitableItems.length - 1; j >= 0; j--) {
                        var gravitableItem = gravitableItems[j];
                        this.hold(planet, gravitableItem, time, game);
                    }
                }
            };
            PlanetHoldsItemsRule.hold = function (planet, item, elapsedTime, game) {
                var itemPosition = this.getDestination(item, elapsedTime), distance = planet.position.getDistanceFrom(itemPosition), distanceFromSurface = distance - planet.radius;
                if (distance <= planet.radius) {
                    var newPosition = this.getLineIntersection(planet, itemPosition, item.speed.add(item.moveSpeed));
                    if (newPosition !== undefined) {
                        item.position = newPosition;
                        item.speed = new PlanetWars.Utils.Vector();
                        Model.ItemsCollidesWithPlanetRule.execute(item, planet, game);
                    }
                }
            };
            PlanetHoldsItemsRule.getDestination = function (item, elapsedTime) {
                if (Model.Player.isPlayer(item)) {
                    return Model.GetPlayerDestinationRule.execute(item, elapsedTime);
                }
                if (Model.Projectile.isProjectile(item)) {
                    return Model.GetProjectileDestinationRule.execute(item, elapsedTime);
                }
            };
            PlanetHoldsItemsRule.getLineIntersection = function (planet, p0, v) {
                var r = planet.radius, p1 = PlanetWars.Utils.Vector.vectorBetween(planet.position, p0), p2 = p1.add(v), dx = p2.x - p1.x, dy = p2.y - p1.y, dr = Math.sqrt(dx * dx + dy * dy), D = p1.x * p2.y - p2.x * p1.y, inc = r * r * dr * dr - (D * D), x, y;
                if (inc > 0) {
                    x = (D * dy + sgn(dy) * dx * Math.sqrt(inc)) / (dr * dr);
                    y = (-D * dx + Math.abs(dy) * Math.sqrt(inc)) / (dr * dr);
                    var incPoint1 = new PlanetWars.Utils.Vector(x, y);
                    x = (D * dy - sgn(dy) * dx * Math.sqrt(inc)) / (dr * dr);
                    y = (-D * dx - Math.abs(dy) * Math.sqrt(inc)) / (dr * dr);
                    var incPoint2 = new PlanetWars.Utils.Vector(x, y);
                    var selectedIncPoint = selectBetweenIncPoints(incPoint1, incPoint2, p1);
                    return planet.position.add(selectedIncPoint);
                }
                function sgn(x) {
                    if (x < 0)
                        return -1;
                    return 1;
                }
                function selectBetweenIncPoints(incPoint1, incPoint2, refPoint) {
                    var d1 = incPoint1.getDistanceFrom(refPoint), d2 = incPoint2.getDistanceFrom(refPoint);
                    if (d1 < d2)
                        return incPoint1;
                    return incPoint2;
                }
            };
            return PlanetHoldsItemsRule;
        })();
        Model.PlanetHoldsItemsRule = PlanetHoldsItemsRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var GetPlayerDestinationRule = (function () {
            function GetPlayerDestinationRule() {
            }
            GetPlayerDestinationRule.execute = function (player, elapsedTime) {
                var _speed = player.speed.add(player.jumpSpeed), _position = player.position.add(_speed.add(player.moveSpeed).mul(elapsedTime));
                return _position;
            };
            return GetPlayerDestinationRule;
        })();
        Model.GetPlayerDestinationRule = GetPlayerDestinationRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var GetProjectileDestinationRule = (function () {
            function GetProjectileDestinationRule() {
            }
            GetProjectileDestinationRule.execute = function (projectile, elapsedTime) {
                var speed = projectile.speed, position = projectile.position.add(projectile.speed.add(projectile.moveSpeed).mul(elapsedTime));
                return position;
            };
            return GetProjectileDestinationRule;
        })();
        Model.GetProjectileDestinationRule = GetProjectileDestinationRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var ItemsCollidesWithPlanetRule = (function () {
            function ItemsCollidesWithPlanetRule() {
            }
            ItemsCollidesWithPlanetRule.execute = function (item, planet, game) {
                if (Model.Player.isPlayer(item)) {
                    this.playerCollidesWithPlanet(item);
                }
                if (Model.Projectile.isProjectile(item)) {
                    this.projectileCollidesWithPlanet(item, game);
                }
            };
            ItemsCollidesWithPlanetRule.playerCollidesWithPlanet = function (player) {
                player.isInJump = false;
                player.jumpSpeed = new PlanetWars.Utils.Vector();
            };
            ItemsCollidesWithPlanetRule.projectileCollidesWithPlanet = function (projectile, game) {
                Model.RemoveProjectileRule.execute(projectile, game);
            };
            return ItemsCollidesWithPlanetRule;
        })();
        Model.ItemsCollidesWithPlanetRule = ItemsCollidesWithPlanetRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var MovePlayersRule = (function () {
            function MovePlayersRule() {
            }
            MovePlayersRule.execute = function (time, game) {
                var players = game.players;
                for (var i = players.length - 1; i >= 0; i--) {
                    var player = players[i];
                    this.move(player, time);
                }
                ;
            };
            MovePlayersRule.move = function (player, elapsedTime) {
                player.speed = player.speed.add(player.jumpSpeed);
                player.position = player.position.add(player.speed.add(player.moveSpeed).mul(elapsedTime));
                player.jumpSpeed = new PlanetWars.Utils.Vector();
                player.moveSpeed = new PlanetWars.Utils.Vector();
            };
            return MovePlayersRule;
        })();
        Model.MovePlayersRule = MovePlayersRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var MoveProjectilesRule = (function () {
            function MoveProjectilesRule() {
            }
            MoveProjectilesRule.execute = function (elapsedTime, game) {
                var items = game.projectiles;
                for (var i = items.length - 1; i >= 0; i--) {
                    var item = items[i];
                    this.move(item, elapsedTime);
                    Model.ProjectilesCanHitPlayersRule.execute(item, elapsedTime, game);
                }
                ;
            };
            MoveProjectilesRule.move = function (projectile, elapsedTime) {
                var prevPos = projectile.position;
                projectile.position = projectile.position.add(projectile.speed.add(projectile.moveSpeed).mul(elapsedTime));
                var movingVector = PlanetWars.Utils.Vector.vectorBetween(prevPos, projectile.position).normalize();
                projectile.angle = movingVector.getAngleFromVerticalUp();
            };
            return MoveProjectilesRule;
        })();
        Model.MoveProjectilesRule = MoveProjectilesRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var ProjectilesCanHitPlayersRule = (function () {
            function ProjectilesCanHitPlayersRule() {
            }
            ProjectilesCanHitPlayersRule.execute = function (projectile, elapsedTime, game) {
                this.checkCollision(projectile, elapsedTime, game);
            };
            ProjectilesCanHitPlayersRule.checkCollision = function (projectile, elapsedTime, game) {
                var players = game.players;
                for (var i = players.length - 1; i >= 0; i--) {
                    var player = players[i];
                    if (this.isCollide(player, projectile, elapsedTime)) {
                        this.collide(player, projectile, game);
                    }
                }
            };
            ProjectilesCanHitPlayersRule.isCollide = function (player, item, elapsedTime) {
                var itemDestination = Model.GetProjectileDestinationRule.execute(item, elapsedTime), itemPosition = itemDestination.add(player.position.mul(-1)), itemsRelativePosition = itemPosition.rotate(-PlanetWars.Utils.DegRadConverter.ConvertToDeg(player.getUp().getAngleFromVerticalUp()));
                if ((Math.abs(itemsRelativePosition.x) < (player.size.width / 2))) {
                    if (((itemsRelativePosition.y < 0) && (itemsRelativePosition.y >= -player.size.height))) {
                        return true;
                    }
                }
                else {
                    return false;
                }
            };
            ProjectilesCanHitPlayersRule.collide = function (player, projectile, game) {
                if (!player.isInvulnerable) {
                    this.hit(player);
                    projectile.owner.points += Model.settings.game.pointsOnShot;
                    projectile.owner.group.score += Model.settings.game.pointsOnShot;
                    Model.RemoveProjectileRule.execute(projectile, game);
                }
            };
            ProjectilesCanHitPlayersRule.hit = function (player) {
                player.hp -= Model.settings.projectile.demage;
                if (player.hp === 0) {
                    this.dead(player);
                }
                else {
                    this.shot(player);
                }
            };
            ProjectilesCanHitPlayersRule.shot = function (player) {
                Model.PlayersBecomeInvulnerableAfterShot.execute(player);
            };
            ProjectilesCanHitPlayersRule.dead = function (player) {
                Model.PlayersBecomeInvulnerableAfterDeath.execute(player);
                player.points -= Model.settings.game.minusPointsOnDeath;
                player.group.score -= Model.settings.game.minusPointsOnDeath;
                player.hp = Model.settings.player.maxHP;
            };
            return ProjectilesCanHitPlayersRule;
        })();
        Model.ProjectilesCanHitPlayersRule = ProjectilesCanHitPlayersRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var RemoveProjectileRule = (function () {
            function RemoveProjectileRule() {
            }
            RemoveProjectileRule.execute = function (projectile, game) {
                Model.IIdentifiableUtils.removeFromArrayById(projectile.id, game.gravitableItems);
                Model.IIdentifiableUtils.removeFromArrayById(projectile.id, game.projectiles);
            };
            return RemoveProjectileRule;
        })();
        Model.RemoveProjectileRule = RemoveProjectileRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var PlayersBecomeInvulnerableAfterShot = (function () {
            function PlayersBecomeInvulnerableAfterShot() {
            }
            PlayersBecomeInvulnerableAfterShot.execute = function (player) {
                this.startInvulnerability(player);
            };
            PlayersBecomeInvulnerableAfterShot.startInvulnerability = function (player) {
                player.isInvulnerable = true;
                setTimeout(function () { return PlayersBecomeInvulnerableAfterShot.endInvulnerability(player); }, Model.settings.player.InvulnerabilityTimeoutAfterShot);
            };
            PlayersBecomeInvulnerableAfterShot.endInvulnerability = function (player) {
                player.isInvulnerable = false;
            };
            return PlayersBecomeInvulnerableAfterShot;
        })();
        Model.PlayersBecomeInvulnerableAfterShot = PlayersBecomeInvulnerableAfterShot;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var PlayersBecomeInvulnerableAfterConnect = (function () {
            function PlayersBecomeInvulnerableAfterConnect() {
            }
            PlayersBecomeInvulnerableAfterConnect.execute = function (player) {
                this.startInvulnerability(player);
            };
            PlayersBecomeInvulnerableAfterConnect.startInvulnerability = function (player) {
                player.isInvulnerable = true;
                setTimeout(function () { return PlayersBecomeInvulnerableAfterConnect.endInvulnerability(player); }, Model.settings.player.InvulnerabilityTimeoutAfterConnect);
            };
            PlayersBecomeInvulnerableAfterConnect.endInvulnerability = function (player) {
                player.isInvulnerable = false;
            };
            return PlayersBecomeInvulnerableAfterConnect;
        })();
        Model.PlayersBecomeInvulnerableAfterConnect = PlayersBecomeInvulnerableAfterConnect;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var PlayersBecomeInvulnerableAfterDeath = (function () {
            function PlayersBecomeInvulnerableAfterDeath() {
            }
            PlayersBecomeInvulnerableAfterDeath.execute = function (player) {
                this.startInvulnerability(player);
            };
            PlayersBecomeInvulnerableAfterDeath.startInvulnerability = function (player) {
                player.isInvulnerable = true;
                setTimeout(function () { return PlayersBecomeInvulnerableAfterDeath.endInvulnerability(player); }, Model.settings.player.InvulnerabilityTimeoutAfterDeath);
            };
            PlayersBecomeInvulnerableAfterDeath.endInvulnerability = function (player) {
                player.isInvulnerable = false;
            };
            return PlayersBecomeInvulnerableAfterDeath;
        })();
        Model.PlayersBecomeInvulnerableAfterDeath = PlayersBecomeInvulnerableAfterDeath;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var ProjectilesAgingRule = (function () {
            function ProjectilesAgingRule() {
            }
            ProjectilesAgingRule.execute = function (time, game) {
                var items = game.projectiles;
                for (var i = items.length - 1; i >= 0; i--) {
                    var item = items[i];
                    this.ageProjectile(item, time, game);
                }
                ;
            };
            ProjectilesAgingRule.ageProjectile = function (projectile, elapsedTime, game) {
                if (projectile.lifeTime < 0) {
                    Model.RemoveProjectileRule.execute(projectile, game);
                }
                else {
                    projectile.lifeTime -= elapsedTime;
                }
            };
            return ProjectilesAgingRule;
        })();
        Model.ProjectilesAgingRule = ProjectilesAgingRule;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Model;
    (function (Model) {
        var Simulator = (function () {
            function Simulator(game) {
                this.game = game;
            }
            Simulator.prototype.startSimulate = function () {
                var _this = this;
                this.previousTime = PlanetWars.Utils.Timer.getTime();
                setInterval(function () {
                    _this.simulate();
                }, Model.settings.game.refreshRate);
            };
            Simulator.prototype.simulate = function () {
                this.elapsedTime = this.getElapsedTime();
                this.calculatePlayersMovement();
                this.fireProjectiles();
                this.gravitateItems();
                this.holdItems();
                this.moveItems();
                this.ageProjectiles();
            };
            Simulator.prototype.getElapsedTime = function () {
                var time = PlanetWars.Utils.Timer.getTime(), elapsedTime = PlanetWars.Utils.Timer.getElapsedTimeInSec(time, this.previousTime);
                this.previousTime = time;
                this.time = time;
                return elapsedTime;
            };
            Simulator.prototype.calculatePlayersMovement = function () {
                Model.CalculatePlayersMovementRule.execute(this.game);
            };
            Simulator.prototype.fireProjectiles = function () {
                Model.FireProjectilesRule.execute(this.time, this.game);
            };
            Simulator.prototype.gravitateItems = function () {
                Model.PlanetsGravitateRule.execute(this.elapsedTime, this.game);
            };
            Simulator.prototype.holdItems = function () {
                Model.PlanetHoldsItemsRule.execute(this.elapsedTime, this.game);
            };
            Simulator.prototype.moveItems = function () {
                Model.MovePlayersRule.execute(this.elapsedTime, this.game);
                Model.MoveProjectilesRule.execute(this.elapsedTime, this.game);
            };
            Simulator.prototype.ageProjectiles = function () {
                Model.ProjectilesAgingRule.execute(this.elapsedTime, this.game);
            };
            return Simulator;
        })();
        Model.Simulator = Simulator;
    })(Model = PlanetWars.Model || (PlanetWars.Model = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\definitions\node.d.ts' />
///<reference path='..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Server;
    (function (Server) {
        var GameServer = (function () {
            function GameServer() {
                GameServer.current = this;
                this.initGame();
                this.initWebServer();
                this.initCommunication();
            }
            GameServer.Start = function () {
                new GameServer();
            };
            GameServer.prototype.initGame = function () {
                this.modelController = PlanetWars.Model.GameFactory.newGame();
            };
            GameServer.prototype.initWebServer = function () {
                this.webServer = new Server.WebServer();
                this.webServer.init({
                    port: Server.Settings.WebServer.port,
                    defaultFileName: Server.Settings.WebServer.startPage
                });
            };
            GameServer.prototype.initCommunication = function () {
                var _this = this;
                this.players = {};
                var that = this;
                this.io = this.webServer.getSocketIO();
                this.io.use(function (socket, next) {
                    var player = that.players[socket.id];
                    if (typeof player !== "undefined") {
                        socket.player = this.players[socket.id];
                    }
                    next();
                });
                this.io.sockets.on(Server.Communication.messages.connection, function (socket) {
                    var msg = Server.Communication.messages;
                    var dir = Server.Communication.directions;
                    _this.initCreateNewPlayer(socket);
                    _this.initRefreshRequest(socket);
                    _this.initMessage(socket, msg.startMove, _this.onStartMove);
                    _this.initMessage(socket, msg.endMove, _this.onEndMove);
                    _this.initMessage(socket, msg.disconnect, _this.onDisconnect);
                    _this.initMessage(socket, msg.startFire, _this.onStartFire);
                    _this.initMessage(socket, msg.endFire, _this.onEndFire);
                });
            };
            GameServer.prototype.initCreateNewPlayer = function (socket) {
                var _this = this;
                var msg = Server.Communication.messages, that = this;
                socket.on(msg.createNewPlayer, function (args) {
                    var player = that.modelController.gameController.createNewPlayer(args.name, args.group);
                    var planetDtos = that.modelController.modelQueries.getPlanetsDto();
                    console.log(args.name + " connected!");
                    //socket.set(Settings.Communication.playerStorageName, player);
                    _this.players[socket.id] = player;
                    socket.player = player;
                    socket.emit(Server.Communication.messages.newPlayerCreated, {
                        id: player.id,
                        p: planetDtos
                    });
                });
            };
            GameServer.prototype.initRefreshRequest = function (socket) {
                var msg = Server.Communication.messages, that = this;
                socket.on(msg.refreshRequest, function (data) {
                    var player = socket.player;
                    if (player !== null) {
                        player.fireAngle = data.angle;
                        var redscore = that.modelController.modelQueries.getScoreForGroup(0);
                        var bluescore = that.modelController.modelQueries.getScoreForGroup(1);
                        var items = that.modelController.modelQueries.getDtosForPlayer(player);
                        socket.emit(msg.refresh, {
                            s: data.sendTime,
                            x: Math.round(player.position.x),
                            y: Math.round(player.position.y),
                            r: redscore,
                            b: bluescore,
                            i: items
                        });
                    }
                });
            };
            GameServer.prototype.initMessage = function (socket, message, callback) {
                var that = this;
                socket.on(message, function (data) {
                    that.loadPlayer(socket, function (err, player) {
                        if (player !== null) {
                            callback.call(that, err, player, data);
                        }
                    });
                });
            };
            GameServer.prototype.onStartMove = function (err, player, data) {
                var dir = Server.Communication.directions;
                if (data.d === dir.up)
                    this.modelController.playerController.startMoveUp(player);
                if (data.d === dir.left)
                    this.modelController.playerController.startMoveLeft(player);
                if (data.d === dir.right)
                    this.modelController.playerController.startMoveRight(player);
            };
            GameServer.prototype.onEndMove = function (err, player, data) {
                var dir = Server.Communication.directions;
                if (data.d === dir.up)
                    this.modelController.playerController.endMoveUp(player);
                if (data.d === dir.left)
                    this.modelController.playerController.endMoveLeft(player);
                if (data.d === dir.right)
                    this.modelController.playerController.endMoveRight(player);
            };
            GameServer.prototype.onDisconnect = function (err, player, data) {
                console.log(player.name + " disconnected!");
                this.modelController.gameController.removePlayer(player);
            };
            GameServer.prototype.onStartFire = function (err, player, data) {
                this.modelController.playerController.startFire(player);
            };
            GameServer.prototype.onEndFire = function (err, player, data) {
                this.modelController.playerController.endFire(player);
            };
            GameServer.prototype.savePlayer = function (socket, player) {
                //socket.player = player;
            };
            GameServer.prototype.loadPlayer = function (socket, callback) {
                callback(null, socket.player);
            };
            GameServer.publishPlanetAdded = function (planet) {
                if (typeof this.current.io !== "undefined") {
                    this.current.io.sockets.emit(Server.Communication.messages.planetAddedName, planet.toDto());
                }
            };
            GameServer.publishPlanetRemoved = function (planet) {
                if (typeof this.current.io !== "undefined") {
                    if (typeof planet !== "undefined")
                        this.current.io.sockets.emit(Server.Communication.messages.planetRemovedName, planet.toDto());
                }
            };
            return GameServer;
        })();
        Server.GameServer = GameServer;
    })(Server = PlanetWars.Server || (PlanetWars.Server = {}));
})(PlanetWars || (PlanetWars = {}));
var PlanetWars;
(function (PlanetWars) {
    var Server;
    (function (Server) {
        var WebServer = (function () {
            function WebServer() {
                this.express = require('express');
                this.app = this.express();
                this.server = require('http').createServer(this.app);
                this.io = require('socket.io').listen(this.server);
            }
            WebServer.prototype.init = function (configuration) {
                var port = process.env.PORT || 8090, fileName = configuration.defaultFileName || '/index.html';
                this.initialiseHttpServer(port);
                this.configureHttpServer(fileName);
                //this.io.enable('browser client gzip');
                //this.io.set('transports', [
                //            'websocket',
                //            'flashsocket'
                //]);
                //this.io.enable('browser client minification');  // send minified client
                //this.io.set('log level', 1);                    // reduce logging
            };
            WebServer.prototype.getSocketIO = function () {
                return this.io;
            };
            WebServer.prototype.initialiseHttpServer = function (port) {
                this.server.listen(port);
                console.log("Server is listening in " + port);
            };
            WebServer.prototype.handleGetCompleted = function (error) {
                if (error) {
                    this.handleGetError(error);
                }
                else {
                    this.handleGetSuccess();
                }
            };
            WebServer.prototype.handleGetError = function (error) {
                console.error("Routing error: " + error);
            };
            WebServer.prototype.handleGetSuccess = function () {
                console.log("Get was successful");
            };
            WebServer.prototype.configureHttpServer = function (fileName) {
                this.app.use(this.express.static(__dirname + '/public'));
            };
            return WebServer;
        })();
        Server.WebServer = WebServer;
    })(Server = PlanetWars.Server || (PlanetWars.Server = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Server;
    (function (Server) {
        var Settings;
        (function (Settings) {
            Settings.WebServer = {
                port: 8080,
                startPage: '/index.html'
            };
            Settings.Communication = {
                playerStorageName: 'player'
            };
        })(Settings = Server.Settings || (Server.Settings = {}));
    })(Server = PlanetWars.Server || (PlanetWars.Server = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='..\..\PlanetWarsServer.ts' />
var PlanetWars;
(function (PlanetWars) {
    var Server;
    (function (Server) {
        var Communication;
        (function (Communication) {
            Communication.messages = {
                connection: 'connection',
                disconnect: 'disconnect',
                createNewPlayer: 'p',
                newPlayerCreated: 'P',
                startMove: 'm',
                endMove: 'M',
                startFire: 'f',
                endFire: 'F',
                refreshRequest: 'r',
                refresh: 'r',
                planetAddedName: 'a',
                planetRemovedName: 'A'
            };
            Communication.directions = {
                up: 'u',
                down: 'd',
                left: 'l',
                right: 'r'
            };
        })(Communication = Server.Communication || (Server.Communication = {}));
    })(Server = PlanetWars.Server || (PlanetWars.Server = {}));
})(PlanetWars || (PlanetWars = {}));
///<reference path='Definitions\Definitions.ts' />
///<reference path='Utils\Utils.ts'/>
///<reference path='Model\Model.ts' />
///<reference path='Server\Server.ts' />
PlanetWars.Server.GameServer.Start();
