﻿define(['Require/domReady!', 'Views/Display'], function (document, display) {
    var planetImage = document.getElementById("planet");

    function setShapeRadius(r) {
        radius = r;
    }

    function createShape(x, y, r) {
        display.drawImage(planetImage, x-r, y-r, r*2, r*2);
    }

    return {
        draw: function (x, y, r) {
            createShape(x, y, r);
        }
    }
});