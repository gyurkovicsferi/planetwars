﻿define(['Require/domReady!', 'jQuery/jQuery', 'Settings/Settings'],
    function (document, $, settings) {
        var displayItems = [],
            canvas = document.getElementById(settings.canvasSettings.name),
            ctx = canvas.getContext("2d"),
            layer2 = document.getElementById("targetCanvas"),
            ctx2 = layer2.getContext("2d");

        function getCanvasFullscreen() {
            var width = $(window).width();
            var height = $(window).height();

            $("#" + settings.canvasSettings.name).css("width", width + "px");
            $("#" + settings.canvasSettings.name).css("height", height + "px");

            canvas.width = width;
            canvas.height = height;

            $("#targetCanvas").css("width", width + "px");
            $("#targetCanvas").css("height", height + "px");

            layer2.width = width;
            layer2.height = height;
        };

        $(window).resize(getCanvasFullscreen);

        return {
            getCtx: function () {
                return ctx;
            },

            getCtx2: function() {
                return ctx2;
            },

            eraseDisplay: function () {
                canvas.width = canvas.width;
            },

            eraseDisplay2: function () {
                layer2.width = layer2.width;
            },

            drawImage: function (img, x, y, width, height) {
                ctx.drawImage(img, x, y, width, height);
            },

            drawRect: function (style, x, y, width, height) {
                ctx.fillStyle = style;
                ctx.fillRect(x, y, width, height);
            },

            getCenter: function () {
                return {
                    x: canvas.width / 2,
                    y: canvas.height / 2
                }
            },

            getSize: function () {
                return {
                    width: canvas.width,
                    height: canvas.height
                }
            },

            saveContext: function () {
                ctx.save();
            },

            restoreContext: function () {
                ctx.restore();
            },

            fullscreen: function() {
                getCanvasFullscreen();
            }

        }
    });