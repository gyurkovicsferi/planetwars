//Views namespace
define(['Views/Display', 'Views/PlayerView', 'Views/PlanetView', 'Views/ProjectileView', 'Views/ScoreboardView', 'Views/TargetView'], function (display, playerView, planetView, projectileView, scoreboardView, targetView) {
    
    function drawScene(planets, items, player, redScore, blueScore) {
        var players = [];
            center = display.getCenter();

        display.eraseDisplay();

        for (var i = planets.length - 1; i >= 0; i--) {
            var planet = planets[i];
            planetView.draw(center.x - (player.x - planet.x), center.y - (player.y - planet.y), planet.r);
        }

        for (i = items.length - 1; i >= 0; i--) {
            var item = items[i];
            var shape,
                type = item.t;

            if (type === 'y') {
                var name = item.n,
                     angle = item.a,
                     hp = item.h,
                     group = item.g,
                     invulerable = item.i;
                playerView.draw(name, center.x - item.x, center.y - item.y, angle, hp, invulerable, group);
                players.push(item);
            }

            if (type === 'j') {
                projectileView.draw(center.x - item.x, center.y - item.y, item.a);
            }
        }


        scoreboardView.draw(players, redScore, blueScore);
    }

    function fullscreen() {
        display.fullscreen();
    }

    function getCenter() {
        return display.getCenter();
    }

    return {
        'playerView': playerView,
        'planetView': planetView,
        'projectileView': projectileView,
        'scoreBoardView': scoreboardView,
        'targetView': targetView,
        'fullscreen': fullscreen,
        'getCenter': getCenter,
        'drawScene': drawScene
    }
});