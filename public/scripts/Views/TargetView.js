﻿define(['Require/domReady!', 'Views/Display'], function (document, display) {
    var image = document.getElementById("target");

    var ctx = display.getCtx2();

    function createShape(x, y) {
        //console.log(image);

        display.eraseDisplay2();
        ctx.save();

        ctx.translate(x, y);
        ctx.drawImage(image, 0, 0, 30, 29);

        ctx.restore();
    }

    return {
        draw: function (x, y) {
            createShape(x, y);
        }
    }
});