﻿define(['Require/domReady!', 'Views/Display'], function (document, display) {
    var bulettImage = document.getElementById("bullet");

    function createShape(x, y, angle) {
        var ctx = display.getCtx();
        ctx.save();

        ctx.translate(x, y);
        ctx.rotate(((angle - 90) / 180 * Math.PI));

        display.drawImage(bulettImage, -38, -7, 45, 15);

        ctx.restore();

    }

    return {
        draw: function (x, y, angle) {
            createShape(x, y, angle);
        }
    }
});