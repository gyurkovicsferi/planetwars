﻿define(['Require/domReady!', 'Views/Display'], function (document, display) {
    var playerRedImage = document.getElementById("playerred");
    var playerBlueImage = document.getElementById("playerblue");
    var playerRedInvImage = document.getElementById("playerredinv");
    var playerBlueInvImage = document.getElementById("playerblueinv");
    var ctx = display.getCtx();

    function createShape(name, x, y, angle, hp, isInvulnerable, group) {
        ctx.save();

        ctx.translate(x, y);
        ctx.rotate(angle / 180 * Math.PI);

        if (group === 0) {

            ctx.fillStyle = 'red';
            ctx.fillRect(-5, -60, 2 * hp, 5);

            if (!isInvulnerable) {
                display.drawImage(playerRedImage, -29, -47, 60, 52);
            } else {
                display.drawImage(playerRedInvImage, -29, -47, 60, 52);
            }
        } else if (group === 1) {

            ctx.fillStyle = 'blue';
            ctx.fillRect(-5, -60, 2 * hp, 5);

            if (!isInvulnerable) {
                display.drawImage(playerBlueImage, -29, -47, 60, 52);
            } else {
                display.drawImage(playerBlueInvImage, -29, -47, 60, 52);
            }
        }

        ctx.fillStyle = "#EEFFFF";

        ctx.font = "normal 14px Arial";
        ctx.textAlign = "center";
        ctx.fillText(name, 5, -70);


        ctx.restore();
    }

    return {
        draw: function (name, x, y, angle, hp, isInvulnerable, group) {
            createShape(name, x, y, angle, hp, isInvulnerable, group);
        }
    };
});