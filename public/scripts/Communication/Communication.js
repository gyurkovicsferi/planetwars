define(['Communication/SocketIO', 'Settings/Settings'], function (io, Settings) {
    var messageTypes = {   
            connection: 'connection',
            disconnect: 'disconnect',
            createNewPlayer: 'p',
            newPlayerCreated: 'P',
            startMove: 'm',
            endMove: 'M',
            startFire: 'f',
            endFire: 'F',
            refreshRequest: 'r',
            refresh: 'r',
            planetAddedName: 'a',
            planetRemovedName: 'A'
    };


    var socket = io.connect();

    var subscribe = function (messageType, callback) {
        socket.on(messageType, callback);
    };

    var publish = function (messageType, data) {
        socket.emit(messageType, data);
    };


    return {
        messageTypes: messageTypes,
        publish: publish,
        subscribe: subscribe
    }
});
