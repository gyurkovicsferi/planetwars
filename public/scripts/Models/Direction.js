define(['Models/Keymapping'], function (keyMapping) {
    return {
        left: 'l',
        right: 'r',
        up: 'u',
        down: 'd',

        fromKey: function (key) {
            switch (key.toUpperCase()) {
                case keyMapping.left:
                    return this.left;
                case keyMapping.right:
                    return this.right;
                case keyMapping.up:
                    return this.up;
                case keyMapping.down:
                    return this.down;
            }
        }
    };
});