define(['Communication/Communication'], function (communication) {
    var id = 0;
    var shape = {}

    return {
        aimAngle: 0,

        x: 0,

        y: 0,

        startMove: function (direction) {
            communication.publish(communication.messageTypes.startMove, {
                d: direction
            });
        },

        endMove: function (direction) {
            communication.publish(communication.messageTypes.endMove, {
                d: direction
            });
        },

        startFire: function () {
            communication.publish(communication.messageTypes.startFire, {

            });
        },

        endFire: function () {
            communication.publish(communication.messageTypes.endFire, {

            });
        },

        aim: function (x, y) {
            var factor;
            if (x < 0) {
                factor = -1;
            } else {
                factor = 1;
            }

            this.aimAngle = factor * Math.acos(-y / Math.sqrt(x * x + y * y)) * 180 / Math.PI;
        },

        joinToGroup: function (name, id) {
            communication.publish(communication.messageTypes.createNewPlayer, {
                name: name,
                group: id
            });
        }

    };
});
