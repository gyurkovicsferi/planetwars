requirejs.config({
    baseUrl: 'scripts',
    paths: {
        'jquery': 'jquery-1.7.2.min',
        'socket.io': '../socket.io/socket.io.js'
    }
});

requirejs(
    ['Require/domReady!', 'jQuery/jQuery', 'Communication/Communication', 'Models/Direction', 'Models/Player', 'Views/Views'],
    function (document, $, communication, direction, player, views) {
        var msg = communication.messageTypes;

        var initTime = new Date().getTime();
        //setInterval(function () {
        //    initTime = new Date().getTime();
        //}, 2000);

        var planets = [];

        var debug = {
            roundupTimeSum: 0,
            roundCount: 0
        };

        setInterval(function () {
            console.log("Av. lat.: " + (debug.roundupTimeSum / debug.roundCount) + "ms");
        }, 2000);

        function onKeyDown(event) {
            var keyCode = event.which || event.keyCode,
                keyLetter = String.fromCharCode(keyCode),
                dir = direction.fromKey(keyLetter);

            player.startMove(dir);
        }

        function onKeyUp(event) {
            var keyCode = event.which || event.keyCode,
                keyLetter = String.fromCharCode(keyCode),
                dir = direction.fromKey(keyLetter);

            player.endMove(dir);
        }

        function onMouseDown(event) {
            player.startFire();
        }

        function onMouseUp(event) {
            player.endFire();
        }


        function onMouseMove(event) {
            views.targetView.draw(event.pageX, event.pageY);
            player.aim(event.pageX - views.getCenter().x, event.pageY - views.getCenter().y);
        }

        function onRefreshRecieved(data) {
            var items = data.i,
                currentTime = new Date().getTime() - initTime,
                roundupTime = currentTime - data.s;

            player.x = data.x;
            player.y = data.y;

            views.drawScene(planets, items, player, data.r, data.b);

            debug.roundCount += 1;
            debug.roundupTimeSum += roundupTime;
        }


        function initCommunication() {
            communication.subscribe(msg.refresh, onRefreshRecieved);
            communication.subscribe(msg.planetAddedName, onPlanetAddedRecieved);
            communication.subscribe(msg.planetRemovedName, onPlanetRemovedRecieved);
        }

        communication.subscribe(msg.newPlayerCreated, function (data) {
            player.id = data.id;
            planets = data.p;

            initCommunication();

            $(document).keydown(onKeyDown);
            $(document).keyup(onKeyUp);
            $(document).mousedown(onMouseDown);
            $(document).mouseup(onMouseUp);
            $(document).mousemove(onMouseMove);

            setInterval(function (event) {
                var currentTime = new Date(),
                    sendTime = currentTime.getTime() - initTime;

                communication.publish(msg.refreshRequest, {
                    angle: player.aimAngle,
                    sendTime: sendTime
                });
            }, 30);

        });

        $("#joinBlueButton").click(function () {
            var name = $("#name").val();
            player.joinToGroup(name, 1);
            initcanvas();
        });


        $("#joinRedButton").click(function () {
            var name = $("#name").val();
            player.joinToGroup(name, 0);
            initcanvas();
        });

        function initcanvas() {
            $("#login").hide();
            $("#myCanvas").show();
            $("#targetCanvas").show();
            views.fullscreen();
        }

        function onPlanetRemovedRecieved(data) {
            for (var i = planets.length - 1; i >= 0; i--) {
                if (planets[i].x === data.x && planets[i].y === data.y) break;
            }
            planets.splice(i, 1);
        }

        function onPlanetAddedRecieved(data) {
            planets.push(data);
        }

    });