/* settings.js */
define(function () {
    return {
        serverSettings: {
            prefix: "http://",
            ip: "80.98.104.129", //"80.98.104.129",
            port: "3000",

            getAddress: function () {
                return this.prefix + this.ip + ":" + this.port;
            }
        },

        canvasSettings: {
            name: "myCanvas"
        }
    }
});